<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
header('Content-type: application/x-www-form-urlencoded');
require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

$json = file_get_contents('php://input');
$obj = json_decode($json, true);

$message = trim($obj['message']);
$image = $obj['image'];
$teacherid = $obj['teacherid'];
$studentid = $obj['studentid'];
$senderid = $obj['senderid'];
$userdata = select($mysqli, "users", "userid = '$senderid'", "1");
$usertype = $userdata['userstype'];
$isread = "1";
if($usertype==1){
	$stmt = $mysqli->prepare("INSERT INTO chatmessage ( studentid, teacherid, message, image, senderid, is_teacherread, createdDate ) VALUES (?,?,?,?,?,?,?)");
	$stmt->bind_param ( "iissiis", $studentid, $teacherid, $message, $image, $senderid, $isread, $date );
	if($stmt->execute ()){
		echo '1';
	}
	else {
		echo '0';
	}
} else {
	$stmt = $mysqli->prepare("INSERT INTO chatmessage ( studentid, teacherid, message, image, senderid, is_studentread, createdDate ) VALUES (?,?,?,?,?,?,?)");
	$stmt->bind_param ( "iissiis", $studentid, $teacherid, $message, $image, $senderid, $isread, $date );
	if($stmt->execute ()){
		echo '1';
	}
	else {
		echo '0';
	}
	
}
?>