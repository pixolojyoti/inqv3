<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");


$questionid = $_GET['questionid'];
$userid = $_GET['userid'];
	
	
	$stmt = $mysqli->prepare ( "SELECT answers.answerid, answers.userid, answers.answer, answers.image, answers.verified, answers.verifiedby, answers.createdDate FROM answers INNER JOIN users ON answers.userid = users.userid WHERE question = '$questionid' ORDER BY answers.answerid ASC" );
	if($stmt->execute())
	{
		$stmt->bind_result ( $answerid, $auserid, $answer, $image, $verified, $verifiedby, $createdDate );
		$stmt->store_result ();
		$answersdata = array();
		while($row = $stmt->fetch ()) {
			$userdata = select($mysqli, "users", "userid = '$auserid'", "1");
			$ausertype = $userdata['userstype'];
			$ausername = $userdata['name'];

			$starcnt = cnt($mysqli, "user_staranswer", "answerid = '$answerid'");

			$userstar = 0;
			$userstar = cnt($mysqli, "user_staranswer", "answerid = '$answerid' AND userid = '$userid'");
			if($userstar>0){
				$userstar = 1;
			}
			
			$time = nicetime($createdDate);
			
			$answersdata[] = array(
				'answerid' => $answerid,
				'auserid' => $auserid,
				'answer' => $answer,
				'ausertype' => $ausertype,
				'ausername' => $ausername,
				'image' => $image,
				'time' => $time,
				'verified' => $verified,
				'starcount' => $starcnt,
				'star' => $userstar
			);
			
		}
		header('Content-type: application/json');
		echo json_encode($answersdata);

	}
	else
	{
		echo "0";

	}

?>