<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

$answerid = $_GET['answerid'];
$userid = $_GET['userid'];
$ver = "1";

$quesdata = select($mysqli, "answers", "answerid = '$answerid'", "1");

if($quesdata['verified'] == '1'){
	$ver = "0";
}else{
	$ver = "1";
};

$qid = $quesdata['question'];

	$stmt = $mysqli->prepare("UPDATE answers SET verified = ?, verifiedby = ? WHERE answerid = ?");
	$stmt->bind_param ( "iii", $ver, $userid, $answerid );
	if($stmt->execute ()){
		$stmti = $mysqli->prepare("UPDATE questions SET verified = ?, verifiedby = ? WHERE questionid = ?");
		$stmti->bind_param ( "iii", $ver, $userid, $qid );
		$stmti->execute ();
		echo '1';
	}
	else {
		echo '0';
	}
?>