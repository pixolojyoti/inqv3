<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

/* $userid = $_GET['userid'];
$standardid = $_GET['standardid']; */
$userid = 1;
$standardid = 1;

/* $subjectid = $_GET['subjectid'];
$chapters = $_GET['chapters']; */
/* $number = $_GET['number'];
$limit = $_GET['limit']; */
	
	
	$stmt = $mysqli->prepare ( "SELECT questionid, userid, question, chapterid, image, createdDate FROM questions" );
	
	if($stmt->execute())
	{
		$stmt->bind_result ( $questionid, $quserid, $question, $qchapterid, $image, $qcreatedDate );
		$stmt->store_result ();
		$questionsdata = array();
		while($row = $stmt->fetch ()) {
			$userdata = select($mysqli, "users", "userid = '$quserid'", "1");
			$qusertype = $userdata['userstype'];
			$qusername = $userdata['name'];
			/*$stmti = $mysqli->prepare("SELECT userstype, name FROM users WHERE userid = '$quserid';");
			$stmti->execute ();
			$stmti->bind_result ( $qusertype, $qusername );
			$stmti->store_result ();
			$stmti->fetch ();*/
			
			/* $chapterdata = select($mysqli, "chapter", "chapterid = '$qchapterid'", "1");
			$subjectid = $chapterdata['subjectid']; */
			
			$stmtii = $mysqli->prepare("SELECT subjectid FROM chapter WHERE chapterid = '$qchapterid';");
			$stmtii->execute ();
			$stmtii->bind_result ( $subjectid );
			$stmtii->store_result ();
			$stmtii->fetch ();
			
			/* $subjectdata = select($mysqli, "subject", "subjectid = '$subjectid'", "1");
			$subject = $subjectdata['subject']; */
			
			$stmtiii = $mysqli->prepare("SELECT subjectid FROM subject WHERE subjectid = '$subjectid';");
			$stmtiii->execute ();
			$stmtiii->bind_result ( $subject );
			$stmtiii->store_result ();
			$stmtiii->fetch ();
			
			$answerscount = cnt($mysqli, "answers", "question = '$questionid'");
			
			/* $stmtc = $mysqli->prepare ( "SELECT count(*) as total FROM answers WHERE question = '$questionid'" );
			$stmtc->execute();
			$stmtc->bind_result ( $answerscount );
			$stmtc->store_result ();
			$stmtc->fetch (); */
			
			$verifcnt = cnt($mysqli, "answers", "question = '$questionid' AND verified = '1'");
			/* $stmtci = $mysqli->prepare ( "SELECT count(*) as total FROM answers WHERE question = '$questionid' AND verified = '1'" );
			$stmtci->execute();
			$stmtci->bind_result ( $verifcnt );
			$stmtci->store_result ();
			$stmtci->fetch (); */
			
			if($verifcnt > 0 ){
				$verified = 1;
			}
			else{
				$verified = 0;
			}
			
			$bookmarkedcnt = cnt($mysqli, "users_bookmark", "questionid = '$questionid' AND userid = '$userid'");
			/* $stmtcii = $mysqli->prepare ( "SELECT count(*) as total FROM users_bookmark WHERE questionid = '$questionid' AND userid = '$userid'" );
			$stmtcii->execute();
			$stmtcii->bind_result ( $bookmarkedcnt );
			$stmtcii->store_result ();
			$stmtcii->fetch (); */
			if($bookmarkedcnt > 0 ){
				$bookmarked = 1;
			}
			else{
				$bookmarked = 0;
			}
			$time = nicetime($qcreatedDate);
			
			$questionsdata[] = array(
				'questionid' => $questionid,
				'quserid' => $quserid,
				'question' => $question,
				'qusertype' => $qusertype,
				'qusername' => $qusername,
				'image' => $image,
				'time' => $time,
				'subject' => $subject,
				'subjectid' => $subjectid,
				'qchapterid' => $qchapterid,
				'bookmarked' => $bookmarked,
				'answerscount' => $answerscount,
				'verified' => $verified,
				'bookmarked' => $bookmarked,
			);
			
		}
		header('Content-type: application/json');
		echo json_encode($questionsdata);

	}
	else
	{
		echo "0";

	}

?>