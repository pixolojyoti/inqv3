<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
header('Content-type: application/x-www-form-urlencoded');

require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

$json = file_get_contents('php://input');
$obj = json_decode($json, true);


$question = trim($obj['question']);
$chapterid = $obj['chapterid'];
$userid = $obj['userid'];
$image = $obj['image'];
$imageview = $obj['imageview'];
$video = $obj['video'];
$premium = $obj['premium'];
$trending = $obj['trending'];


/* Remove last of 5 trending questions */
if($trending == 1){

	/*FETCH STANDARD ID THROUGH SUBJECT ID*/
	$subjectidstmt = $mysqli->prepare("SELECT `subjectid` FROM `chapter` WHERE `chapterid` = '$chapterid'");
	$subjectidstmt->execute();
	$subjectidstmt->bind_result($subjectid);
	$subjectidstmt->store_result();

	$standardidstmt = $mysqli->prepare("SELECT `standardid` FROM `subject` WHERE `subjectid` = '$subjectid'");
	$standardidstmt->execute();
	$standardidstmt->bind_result($standardid);
	$standardidstmt->store_result();

	$trendingstmt = $mysqli->prepare("SELECT `questionid` FROM `questions` INNER JOIN `chapter` ON `questions`.`chapterid`=`chapter`.`chapterid` INNER JOIN `subject` ON `chapter`.`subjectid` = `subject`.`subjectid` INNER JOIN `standard` ON `subject`.`standardid` = `standard`.`standardid` WHERE `trending` = '1' AND `standard`.`standardid`='$standardid' ORDER BY `questions`.`createdDate`");
	$trendingstmt->execute();
	$trendingstmt->bind_result ( $questionid);
	$trendingstmt->store_result ();
	$questionsdata = array();
		
	while($row = $trendingstmt->fetch ()) {
		array_push($questionsdata, $questionid);
	}
	/* Number of trending question = 5 */
	if(count($questionsdata)==5){
		$removetrendingstmt = $mysqli->prepare("UPDATE `questions` SET `trending`='0' WHERE `questionid`='$questionsdata[0]'");
		$removetrendingstmt->execute();
	}
}

if(!empty($userid)){
	$userdata = select($mysqli, "users", "userid = '$userid'", "1");
	$type = $userdata['userstype'];
	if($userdata['userstype']==1){
		$ver = 1;
		$stmt = $mysqli->prepare("INSERT INTO questions ( userid, question, chapterid, image, imageview, video, verified, premium, trending, verifiedby, qusertype, createdDate ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
		$stmt->bind_param ( "isisisiiiiis", $userid, $question, $chapterid, $image, $imageview, $video, $ver, $premium, $trending, $userid, $type, $date );
		if($stmt->execute ()){
			echo '1';
		}
		else {
			echo '0';
		}
		
		
	} else {
		$ver = 0;
		$stmt = $mysqli->prepare("INSERT INTO questions ( userid, question, chapterid, image, imageview, video, verified, premium, trending, qusertype, createdDate ) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
		$stmt->bind_param ( "isisisiiiis", $userid, $question, $chapterid, $image, $imageview, $video, $ver, $premium, $trending, $type, $date );
		if($stmt->execute ()){
			echo '1';
		}
		else {
			echo '0';
		}
	}
} else{
	echo "0";
}
	
?>