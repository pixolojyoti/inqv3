<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

$standardid = $_GET['standardid'];

	$status = 0;

	$stmt = $mysqli->prepare ( "SELECT `questions`.`questionid`  FROM `questions` INNER JOIN `chapter` ON `questions`.`chapterid`=`chapter`.`chapterid` INNER JOIN `subject` ON `chapter`.`subjectid` = `subject`.`subjectid` INNER JOIN `standard` ON `subject`.`standardid` = `standard`.`standardid` WHERE `standard`.`standardid` = '$standardid' ORDER BY `questions`.`questionid` DESC LIMIT 0,30");
	if($stmt->execute())
	{
		$stmt->bind_result ($questionid);
		$stmt->store_result ();
		$questionsdata = array();

		while($row = $stmt->fetch ()) {

			$laststmt = $mysqli->prepare("SELECT `users`.`userstype` FROM `users` INNER JOIN `answers` ON `users`.`userid` = `answers`.`userid` WHERE `answers`.`question`=? ORDER BY `answers`.`createdDate` LIMIT 0,1");

				    /* bind parameters for markers */
				    $laststmt->bind_param("i", $questionid);

				    /* execute query */
				    $laststmt->execute();

				    /* bind result variables */
				    $laststmt->bind_result($lastseen);

				    /* fetch value */
				    $laststmt->fetch();

				    /* close statement */
				    $laststmt->close();

				    if($lastseen != '1'){
				    	$status = 1;
				    	break;
				    };

		}
	}

	echo $status;
?>