<?php
	header('Access-Control-Allow-Headers: Content-Type');
	header('Access-Control-Allow-Credentials: true');
	header("Access-Control-Allow-Origin: *");
	header('Access-Control-Max-Age: 86400');
	require_once("connect.php");
	require_once("function.php");

	$userid = $_GET['userid'];
	$standardid = $_GET['standardid'];
	$subjectid = $_GET['subjectid'];
	$chapters = $_GET['chapters'];
	$number = $_GET['number'];
	$limit = $_GET['limit'];
	$search = $_GET['search'];
	$today = date('Y-m-d');
	
	if(empty($subjectid)){
		$filterobj = $_GET['filters'];
		$filter = json_decode($filterobj);
		if($filter != ''){
			if($filter->book==1)
			{
				if(empty($search)){
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions INNER JOIN chapter ON questions.chapterid = chapter.chapterid INNER JOIN subject ON chapter.subjectid = subject.subjectid INNER JOIN users_bookmark ON questions.questionid = users_bookmark.questionid WHERE subject.standardid = '$standardid' AND users_bookmark.userid = '$userid' ORDER BY questions.questionid DESC LIMIT $number, $limit" );
				} else {
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions INNER JOIN chapter ON questions.chapterid = chapter.chapterid INNER JOIN subject ON chapter.subjectid = subject.subjectid INNER JOIN users_bookmark ON questions.questionid = users_bookmark.questionid WHERE subject.standardid = '$standardid' AND users_bookmark.userid = '$userid' AND questions.question LIKE '%$search%' ORDER BY questions.questionid DESC" );
				}
			}
			elseif($filter->me==1)
			{
				if(empty($search)){
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions INNER JOIN chapter ON questions.chapterid = chapter.chapterid INNER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' AND questions.userid = '$userid' ORDER BY questions.questionid DESC LIMIT $number, $limit" );
				} else {
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions INNER JOIN chapter ON questions.chapterid = chapter.chapterid INNER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' AND questions.userid = '$userid' AND questions.question LIKE '%$search%' ORDER BY questions.questionid DESC" );
				}
			}
			elseif($filter->trending==1)
			{
				$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions INNER JOIN chapter ON questions.chapterid = chapter.chapterid INNER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' AND questions.trending = '1' ORDER BY questions.questionid DESC LIMIT $number, $limit" );
				
			}
			 else {
				if(empty($search)){
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions INNER JOIN chapter ON questions.chapterid = chapter.chapterid INNER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' ORDER BY questions.questionid DESC LIMIT $number, $limit" );
				} else {
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions INNER JOIN chapter ON questions.chapterid = chapter.chapterid INNER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' AND questions.question LIKE '%$search%' ORDER BY questions.questionid DESC" );
				}
				
			}
		}else {
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions INNER JOIN chapter ON questions.chapterid = chapter.chapterid INNER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' ORDER BY questions.questionid DESC LIMIT $number, $limit" );
				}
	}
	else {
		$filterobj = $_GET['filters'];
		$filter = json_decode($filterobj);
		if($filter->ans==0 && $filter->unans==0 && $filter->abt==0 && $filter->abst==0 && $filter->askt==0 && $filter->book==0 && $filter->me==0 && $filter->unver==0 && $filter->asks==0)
		{
			if(empty($chapters)){
				if(empty($search)){
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions LEFT OUTER JOIN chapter ON questions.chapterid = chapter.chapterid LEFT OUTER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' AND chapter.subjectid = '$subjectid' ORDER BY questions.questionid DESC LIMIT $number, $limit" );
				} else {
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions LEFT OUTER JOIN chapter ON questions.chapterid = chapter.chapterid LEFT OUTER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' AND chapter.subjectid = '$subjectid' AND questions.question LIKE '%$search%' ORDER BY questions.questionid DESC" );
				}
			} else {
				if(empty($search)){
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions LEFT OUTER JOIN chapter ON questions.chapterid = chapter.chapterid LEFT OUTER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' AND chapter.subjectid = '$subjectid' AND questions.chapterid IN ( $chapters ) ORDER BY questions.questionid DESC LIMIT $number, $limit" );
				} else {
					$stmt = $mysqli->prepare ( "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, questions.verified, questions.imageview, questions.video, questions.premium FROM questions LEFT OUTER JOIN chapter ON questions.chapterid = chapter.chapterid LEFT OUTER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' AND chapter.subjectid = '$subjectid' AND questions.chapterid IN ( $chapters ) AND questions.question LIKE '%$search%' ORDER BY questions.questionid DESC" );
				}
				
			}			
		} else {
			if(empty($chapters)){
				$str = "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, answers.verified, questions.imageview, questions.video, questions.premium  FROM questions LEFT OUTER JOIN answers ON questions.questionid = answers.question LEFT OUTER JOIN users ON answers.userid = users.userid LEFT OUTER JOIN chapter ON questions.chapterid = chapter.chapterid LEFT OUTER JOIN subject ON chapter.subjectid = subject.subjectid LEFT OUTER JOIN users_bookmark ON questions.questionid = users_bookmark.questionid WHERE subject.standardid = '$standardid' AND chapter.subjectid = '$subjectid' ";
				
			} else {
				$str = "SELECT DISTINCT questions.questionid, questions.userid, questions.question, questions.chapterid, questions.image, questions.createdDate, answers.verified, questions.imageview, questions.video, questions.premium  FROM questions LEFT OUTER JOIN answers ON questions.questionid = answers.question LEFT OUTER JOIN users ON answers.userid = users.userid LEFT OUTER JOIN chapter ON questions.chapterid = chapter.chapterid LEFT OUTER JOIN subject ON chapter.subjectid = subject.subjectid LEFT OUTER JOIN users_bookmark ON questions.questionid = users_bookmark.questionid WHERE subject.standardid = '$standardid' AND chapter.subjectid = '$subjectid' AND questions.chapterid IN ( $chapters ) ";
			}
			if(!empty($search)){
				$str.= " AND questions.question LIKE '%$search%' ";
			}
			if($filter->abt==1 || $filter->abst==1 || $filter->askt==1 || $filter->asks==1){
				$str.= "AND (";
			}
			if($filter->abt==1){
				$str.= " answers.ausertype = '1' OR";
			}
			if($filter->abst==1){
				$str.= " answers.ausertype > '1' OR";
			}
			if($filter->askt==1){
				$str.= " questions.qusertype = '1' OR";
			}
			if($filter->asks==1){
				$str.= " questions.qusertype > '1' OR";
			}
			$data[] = substr($str, -2);
			//echo $data[0];
			if($data[0]=="OR")
			{
				$str = substr($str, 0, -2);
				$str.=" ) ";
			}
			if($filter->me==1){
				$str.= "AND questions.userid = '$userid' ";
			}
			if($filter->book==1){
				$str.= "AND users_bookmark.userid = '$userid' ";
			}
			if($filter->unver==1){
				$str.= "AND answers.verified = '0' ";
			}
			if($filter->ans==1 && $filter->unans==1){
				
			}else{
				if($filter->ans==1 || $filter->unans==1){
				$str.= "GROUP BY questions.questionid HAVING ";
				}
				if($filter->ans==1){
					$str.= " count(answers.question) > 0 ";
					if($filter->unans==1){
						$str.= " AND ";
					}
				}
				if($filter->unans==1){
					$str.= " count(answers.question) = 0 ";
				}
			}
			if(empty($search)){
			$str.= "ORDER BY questions.questionid DESC LIMIT $number, $limit";
			} else {
				$str.="";
			}
			print_r($str);
			$stmt = $mysqli->prepare ($str);
		}
		
	}
	


	if($stmt->execute())
	{
		$stmt->bind_result ( $questionid, $quserid, $question, $qchapterid, $image, $qcreatedDate, $verified, $imageview, $video, $premium );
		$stmt->store_result ();
		$questionsdata = array();

		while($row = $stmt->fetch ()) {
			$userdata = select($mysqli, "users", "userid = '$quserid'", "1");
			$qusertype = $userdata['userstype'];
			$qusername = $userdata['name'];
			
			$chapterdata = select($mysqli, "chapter", "chapterid = '$qchapterid'", "1");
			$subjectid = $chapterdata['subjectid'];
			$chapname = $chapterdata['chaptertitle'];
			
			$subjectdata = select($mysqli, "subject", "subjectid = '$subjectid'", "1");
			$subject = $subjectdata['subject'];
			
			$answered = cnt($mysqli, "answers", "question = '$questionid' AND userid = '$userid'");
			if($answered > 0 ){
				$answered = 1;
			}
			else{
				$answered = 0;
			}
			$answerscount = cnt($mysqli, "answers", "question = '$questionid'");

			/* MY STAR VALUE AND TOTAL STAR COUNT */
			$starred = cnt($mysqli, "user_starquestion", "questionid = '$questionid' AND userid = '$userid'");
			if($starred > 0 ){
				$starred = 1;
			}
			else{
				$starred = 0;
			}
			$starcount = cnt($mysqli, "user_starquestion", "questionid = '$questionid'");

			/* MY VIEWED VALUE AND TOTAL VIEW COUNT */
			$viewed = cnt($mysqli, "questionseen", "questionid = '$questionid' AND userid = '$userid'");
			if($viewed > 0 ){
				$viewed = 1;
			}
			else{
				$viewed = 0;
			}
			$viewcount = cnt($mysqli, "questionseen", "questionid = '$questionid'");

			if ($laststmt = $mysqli->prepare("SELECT `users`.`userstype` FROM `users` INNER JOIN `answers` ON `users`.`userid` = `answers`.`userid` WHERE `answers`.`question`=? ORDER BY `answers`.`createdDate` DESC LIMIT 0,1")) {

				    /* bind parameters for markers */
				    $laststmt->bind_param("i", $questionid);

				    /* execute query */
				    $laststmt->execute();

				    /* bind result variables */
				    $laststmt->bind_result($lastseen);

				    /* fetch value */
				    $laststmt->fetch();

				    /* close statement */
				    $laststmt->close();
				}
			
			//$verifcnt = cnt($mysqli, "answers", "question = '$questionid' AND verified = '1'");
			
			/* if($verifcnt > 0 ){
				$verified = 1;
			}
			else{
				$verified = 0;
			} */
			
			$bookmarkedcnt = cnt($mysqli, "users_bookmark", "questionid = '$questionid' AND userid = '$userid'");
			if($bookmarkedcnt > 0 ){
				$bookmarked = 1;
			}
			else{
				$bookmarked = 0;
			}
			$time = nicetime($qcreatedDate);
			
			$creatdate = date('Y-m-d', strtotime($qcreatedDate));
			
			if(strtotime($creatdate)==strtotime($today)){
				$istoday = "1";
			} else{
				$istoday = "0";
			}
			
			$seencount = cnt($mysqli, "questionseen", "userid = '$userid' AND questionid = '$questionid'");
			if($seencount==0){
				$questionseen = "0";
			}else{
				$questionseen = "1";
			}
			
			$questionsdata[] = array(
				'questionid' => $questionid,
				'quserid' => $quserid,
				'question' => $question,
				'qusertype' => $qusertype,
				'qusername' => $qusername,
				'image' => $image,
				'time' => $time,
				'subject' => $subject,
				'subjectid' => $subjectid,
				'qchapterid' => $qchapterid,
				'qchaptername' => $chapname,
				'bookmarked' => $bookmarked,
				'answerscount' => $answerscount,
				'answered' => $answered,
				'starred' => $starred,
				'starcount' => $starcount,
				'viewed' => $viewed,
				'viewcount' => $viewcount,
				'verified' => $verified,
				'imageview' => $imageview,
				'video' => $video,
				'premium' => $premium,
				'istoday' => $istoday,
				'questionseen' => $questionseen,
				'lastanswered' => $lastseen
			);
			
		}
		header('Content-type: application/json');
		echo json_encode($questionsdata);

	}
	else
	{
		echo "0";

	}
?>