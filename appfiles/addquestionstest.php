<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
header('Content-type: application/x-www-form-urlencoded');

require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

$json = file_get_contents('php://input');
$obj = json_decode($json, true);


$question = trim($obj['question']);
$chapterid = $obj['chapterid'];
$userid = $obj['userid'];
$image = $obj['image'];
if(!empty($question) && !empty($userid)){
	$userdata = select($mysqli, "users", "userid = '$userid'", "1");
	if($userdata['userstype']==1){
		$ver = 1;
		$stmt = $mysqli->prepare("INSERT INTO questions ( userid, question, chapterid, image, verified, verifiedby, createdDate ) VALUES (?,?,?,?,?,?,?)");
		$stmt->bind_param ( "isisiis", $userid, $question, $chapterid, $image, $ver, $userid, $date );
		if($stmt->execute ()){
			echo '1';
		}
		else {
			echo '0';
		}
		
		
	} else {
		$ver = 0;
		$stmt = $mysqli->prepare("INSERT INTO questions ( userid, question, chapterid, image, verified, createdDate ) VALUES (?,?,?,?,?,?)");
		$stmt->bind_param ( "isisis", $userid, $question, $chapterid, $image, $ver, $date );
		if($stmt->execute ()){
			echo '1';
		}
		else {
			echo '0';
		}
	}
} else{
	echo "0";
}
	
?>