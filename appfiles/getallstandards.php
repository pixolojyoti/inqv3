<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

	$stmt = $mysqli->prepare ( "SELECT standardid, standard FROM standard ORDER BY standardid ASC" );
	if($stmt->execute())
	{
		$stmt->bind_result ( $standardid, $standard );
		$stmt->store_result ();
		while($row = $stmt->fetch ()) {
			$std = $standard."th";
			//$standarddata = array();
			$standarddata[] = array(
				'standardid' => $standardid,
				'standard' => $std
			);
		}
		header('Content-type: application/json');
		echo json_encode($standarddata);

	}
	else
	{
		echo "0";

	}

?>