<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

	$stmt = $mysqli->prepare ( "SELECT boardid, boardname FROM board ORDER BY boardid ASC" );
	if($stmt->execute())
	{
		$stmt->bind_result ( $boardid, $boardname );
		$stmt->store_result ();
		while($row = $stmt->fetch ()) {
			//$standarddata = array();
			$boarddata[] = array(
				'boardid' => $boardid,
				'boardname' => $boardname
			);
		}
		header('Content-type: application/json');
		echo json_encode($boarddata);

	}
	else
	{
		echo "0";

	}

?>