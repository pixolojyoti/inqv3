<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

$standardid = $_GET['standardid'];

	$stmta = $mysqli->prepare("SELECT todate, fromdate FROM leaderboard ORDER BY lboardid DESC LIMIT 1;");
	$stmta->execute ();
	$stmta->bind_result ( $todate, $fromdate );
	$stmta->store_result ();
	$stmta->fetch ();

	$stmt = $mysqli->prepare ( "SELECT COUNT(questions.questionid) as qcount, questions.userid FROM questions INNER JOIN chapter ON questions.chapterid = chapter.chapterid INNER JOIN subject ON chapter.subjectid = subject.subjectid WHERE subject.standardid = '$standardid' AND questions.verified = '1' AND questions.qusertype > 1 AND questions.createdDate >= '$fromdate' AND questions.createdDate <= '$todate' GROUP BY questions.userid ORDER BY qcount DESC LIMIT 10");
	if($stmt->execute())
	{
		$stmt->bind_result ( $qcount, $userid );
		$stmt->store_result ();
		$qchartdata = array();
		while($row = $stmt->fetch ()) {
			
			$userdata = select($mysqli, "users", "userid = '$userid'", "1");
			$qusername = $userdata['name'];
			$qusertype = $userdata['userstype'];
			
			$qchartdata[] = array(
				'qcount' => $qcount,
				'qusertype' => $qusertype,
				'qusername' => $qusername
			);
			
		}
		header('Content-type: application/json');
		echo json_encode($qchartdata);

	}
	else
	{
		echo "0";

	}

?>