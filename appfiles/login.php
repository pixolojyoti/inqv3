<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');
$ucontact = $_GET['contact'];
$upass = $_GET['password'];
//echo $ucontact;

$stat = "1";

	$stmt = $mysqli->prepare ( "SELECT userid, userstype, userappid, name, email, password, contact, standard, board, school, status, createdDate, updatedDate FROM users WHERE contact = ? AND password = ?" );
	$stmt->bind_param ( "ss", $ucontact, $upass );
	if($stmt->execute())
	{
		$stmt->bind_result ( $userid, $userstype, $userappid, $name, $email, $password, $contact, $standardid, $boardid, $school, $status, $createdDate, $updatedDate);
		$stmt->store_result ();
		$row = $stmt->fetch ();
		if($ucontact==$contact && $upass==$password)
		{
			$stmts = $mysqli->prepare("SELECT standard  FROM standard WHERE standardid = '$standardid';");
			$stmts->execute ();
			$stmts->bind_result ( $standard );
			$stmts->store_result ();
			$stmts->fetch ();
			$std = $standard."th";
			
			$stmtb = $mysqli->prepare("SELECT boardname FROM board WHERE boardid = '$boardid';");
			$stmtb->execute ();
			$stmtb->bind_result ( $boardname );
			$stmtb->store_result ();
			$stmtb->fetch ();
			
			//$userdata = array();
			/* $userdata[] = array(
				'userid' => $userid,
				'userstype' => $userstype,
				'userappid' => $userappid,
				'name' => $name,
				'email' => $email,
				'password' => $password,
				'contact' => $contact,
				'standard' => $std,
				'board' => $board,
				'school' => $school,
				'status' => $status,
				'createdDate' => $createdDate,
				'updatedDate' => $updatedDate
			); */
			
			$userdata['userid'] = $userid;
			$userdata['userstype'] = $userstype;
			$userdata['userappid'] = $userappid;
			$userdata['name'] = $name;
			$userdata['email'] = $email;
			$userdata['password'] = $password;
			$userdata['contact'] = $contact;
			$userdata['standardid'] = $standardid;
			$userdata['standard'] = $std;
			$userdata['boardid'] = $boardid;
			$userdata['boardname'] = $boardname;
			$userdata['school'] = $school;
			$userdata['status'] = $status;
			$userdata['createdDate'] = $createdDate;
			$userdata['updatedDate'] = $updatedDate;
			
			header('Content-type: application/json');
			echo json_encode($userdata);
		}
		else
		{
			echo  "0";
		}

	}
	else
	{
		echo "0";

	}

?>