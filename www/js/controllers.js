var inqapp = angular.module('starter.controllers', []);

var tempuser = {};
var userdata = {};
var offsetvalue = 0;

var tempusersubjects = [];

var edittype;
var editid;
var questionid;
var recieverid;
var recievername;

var usertypes = [
    {
        id: 1,
        'type': 'teacher',
        'image': 'img/feed/teacher.svg'
        },
    {
        id: 2,
        'type': 'Inhouse',
        'image': 'img/feed/inhouse.svg'
        },
    {
        id: 3,
        'type': 'Non In-house',
        'image': 'img/feed/noninhouse.svg'
        }
    ];

var filteredquestion = [];
var filterparameters = {};
var quesfilterapplied = false;
/*var filterparameters = {
    'userid': uid,
                    'standardid': sid,
                    'subjectid': subid,
                    'chapters': chapters,
                    'number': num,
                    'limit': lim,
                    'filters': fil
};*/




inqapp.controller('uploadCtrl', function ($scope, $location, $timeout, $ionicSideMenuDelegate, MyServices, $rootScope, $ionicLoading) {

    /*UPLOAD IMAGE*/

    $scope.$on('$ionicView.beforeEnter', function () {
        // Code you want executed every time view is opened
        $scope.showimg = false;

        var pictureSource; // picture source
        var destinationType; // sets the format of returned value

        document.addEventListener("deviceready", onDeviceReady, false);
    });



    function onDeviceReady() {
        pictureSource = navigator.camera.PictureSourceType;
        destinationType = navigator.camera.DestinationType;
    }

    function clearCache() {
        navigator.camera.cleanup();
    }

    var retries = 0;

    var onCapturePhoto = function (fileURI) {
        console.log(fileURI);
        $scope.fileURI = fileURI;

        /*CALL TO OTHER FUNCTION*/
        $rootScope.$emit("CallParentMethod", {});

        $scope.showimg = true;
        var image = document.getElementById('myImage');
        console.log(image);
        if (image != null) {
            image.src = $scope.fileURI;

            $scope.$apply();
        };

    };
    var onFail = function (message) {
        alert('Failed because: ' + message);
    };

    $scope.capturephoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            allowEdit: true,
            correctOrientation: true
        });
    };
    $scope.pickphoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            correctOrientation: true
        });
    };


    /*FILE TRANSFER*/

    $scope.filetransfer = function () {

        var win = function (data, status) {
            $scope.fileURI = '';
            $ionicLoading.hide();
            $ionicLoading.hide();
            $rootScope.$emit("filetransferdone", data);

        };
        var fail = function (error) {
            $ionicLoading.hide();
            console.log(error);
            MyServices.showToast('There seems to be a problem with the internet');
        };

        console.log($scope.fileURI);
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Uploading Image...'
        });

        $scope.imgname = $scope.fileURI.substr($scope.fileURI.lastIndexOf('/') + 1);

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = $scope.imgname;
        options.mimeType = "image/jpeg";

        var params = {};
        params.value1 = "test";
        params.value2 = "param";

        options.params = params;
        options.chunkedMode = false;

        console.log(options);

        /*if ($scope.fileURI.lastIndexOf('?') > 0) {
            $scope.fileURI.substr(0, $scope.fileURI.lastIndexOf('?'));
        };*/

        var ft = new FileTransfer();
        ft.upload($scope.fileURI, encodeURI("http://learnwithinq.com/appfilesv2/uploadimage.php"), win, fail, options, true);
    };
})
inqapp.controller('AppCtrl', function ($scope, $location, $timeout, $ionicSideMenuDelegate, MyServices, $rootScope, $ionicHistory, $interval, $ionicLoading, $filter) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});




    /*CALLBACK*/



    $scope.gotoprofile = function () {
        $ionicSideMenuDelegate.toggleLeft();
        $location.path("/app/profile");
    };

    $scope.logout = function () {
        console.log("logout");

        var deviceid = $filter('getdeviceid')();
        MyServices.logout(deviceid, $scope.userdata.userid, deviceid).success(function (data, status) {
            console.log(data, status);
        }).error(function (data, status) {
            console.log('There might be a problem with your Internet Connection !');
        });

        $.jStorage.flush();

        for (var i = 0; i < 5; i++) {
            var stateObj = {
                foo: "bar"
            };
            history.pushState(stateObj, "page 2", "index.html#/app/login");
        }
        $location.replace();
        window.location.href = window.location.href + "#";
    };


    var quesleftsuccess = function (data, status) {
        $scope.questionsleft = data.default;
        //        console.log("called");
        $rootScope.questionsleft = data.default;
        $rootScope.paidquestions = data.paid;


    };
    var queslefterror = function (data, status) {
        /**/
    };

    var checkansweredsuccess = function (data, status) {
        if (data == '1') {
            $scope.answered = true;
        } else {
            $scope.answered = false;
        }
    };
    var checkanswerederror = function (data, status) {
        console.log(data);
    };
    var getpendingstatussuccess = function (data, status) {
        if (data == '1') {
            $scope.pending = true;
        } else {
            $scope.pending = false;
        }
    };
    var getpendingstatuserror = function (data, status) {
        console.log(data);
    };
    var checkforans = function () {
        MyServices.checkanswered().success(checkansweredsuccess).error(checkanswerederror);
    };
    var checkforpend = function () {
        if ($scope.userdata != null && $scope.userdata.standardid != '') {
            MyServices.getpendingstatus($scope.userdata.standardid).success(getpendingstatussuccess).error(getpendingstatuserror);
        };
    };
    $rootScope.$on("checkleft", function () {
        checkforleft();
    });
    var checkforleft = function () {
        if ($scope.userdata != null && $scope.userdata.standardid != '') {
            MyServices.quesleft($scope.userdata.userid).success(quesleftsuccess).error(queslefterror);
        };
    };

    $rootScope.showtoast_g = function (msg) {
        MyServices.showToast(msg);
    };

    $scope.$on('$ionicView.beforeEnter', function () {
        // Code you want executed every time view is opened
        $scope.userdata = $.jStorage.get("user");
        $scope.userimage = usertypes;

        $scope.answered = false;
        $scope.pending = false;



        checkforans();
        checkforpend();
        checkforleft();

        refreshans = $interval(function () {
            checkforans();
            checkforpend();
            checkforleft();
        }, 15000, 0);

    });


    /*CALLBACK FUNCTIONS*/

    /*checkpaidquestionssuccess = function (response, status) {
        if (response) {
            //            $rootScope.paidquestion=response.data;

        };


        checkpaidquestionserror = function (response, status) {

        };*/


    $scope.gotoask = function () {
        $ionicSideMenuDelegate.toggleLeft();
        if ($scope.userdata.userstype == 3 && $scope.questionsleft <= 0) {
            //            MyServices.showToast("You cannot ask anymore Questions Today. Wait for tomorrow.");
            //SHOW POP TO USE PAYTM AND PREMIUM QUESTION

            //MyServices.checkpaidquestions($scope.userdata.userid).success(checkpaidquestionssuccess).error(checkpaidquestionserror);

            if ($rootScope.paidquestions > 0) {

                MyServices.confirmationpopupforpaidquestions();

            } else {
                MyServices.showToast("You cannot ask anymore Questions Today. Wait for tomorrow.");
            }





        } else {
            $location.path("/app/ask");
        };
    };

    $scope.refreshfeed = function () {
        $ionicSideMenuDelegate.toggleLeft();
        quesfilterapplied = true;
        filterparameters = {
            'userid': $scope.userdata.userid,
            'standardid': $scope.userdata.standardid,
            'subjectid': '',
            'chapters': '',
            'number': 0,
            'limit': 30,
            'filters': {
                'unver': 0,
                'ans': 0,
                'unans': 0,
                'abt': 0,
                'abst': 0,
                'askt': 0,
                'asks': 0,
                'book': 0,
                'me': 0,
                'trending': 0
            }
        };

        $rootScope.$emit("clearfilters", {});

        if ($location.path() == '/app/questions') {
            $rootScope.$emit("updatequestions", {});
        } else {
            $location.path('/app/questions')
        };

    };

    $scope.gotostd = function () {
        $ionicSideMenuDelegate.toggleLeft();

        $rootScope.$emit("clearfilters", {});
        quesfilterapplied = false;

        $location.path('/app/standard');
    };

    $scope.gotopaq = function () {
        $ionicSideMenuDelegate.toggleLeft();

        quesfilterapplied = true;
        filterparameters = {
            'userid': $scope.userdata.userid,
            'standardid': $scope.userdata.standardid,
            'subjectid': '',
            'chapters': '',
            'number': 0,
            'limit': 20,
            'filters': {
                'unver': 0,
                'ans': 0,
                'unans': 0,
                'abt': 0,
                'abst': 0,
                'askt': 0,
                'asks': 0,
                'book': 0,
                'me': 1,
                'trending': 0
            }
        };
        if ($location.path() == '/app/questions') {
            $rootScope.$emit("updatequestions", {});
        } else {
            $location.path('/app/questions')
        };
    };



    $scope.gotopending = function () {
        $ionicSideMenuDelegate.toggleLeft();

        $rootScope.$emit("clearfilters", {});
        quesfilterapplied = false;
        filterparameters = {
            'userid': $scope.userdata.userid,
            'standardid': $scope.userdata.standardid,
            'subjectid': '',
            'chapters': '',
            'number': 0,
            'limit': 20,
            'filters': {
                'unver': 0,
                'ans': 0,
                'unans': 0,
                'abt': 0,
                'abst': 0,
                'askt': 0,
                'asks': 0,
                'book': 0,
                'me': 0,
                'trending': 0
            }
        };
        $location.path('/app/pending')
    };

    $scope.gotobookmarks = function () {
        $ionicSideMenuDelegate.toggleLeft();

        quesfilterapplied = true;
        filterparameters = {
            'userid': $scope.userdata.userid,
            'standardid': $scope.userdata.standardid,
            'subjectid': '',
            'chapters': '',
            'number': 0,
            'limit': 20,
            'filters': {
                'unver': 0,
                'ans': 0,
                'unans': 0,
                'abt': 0,
                'abst': 0,
                'askt': 0,
                'asks': 0,
                'book': 1,
                'me': 0,
                'trending': 0
            }
        };

        if ($location.path() == '/app/questions') {
            console.log('calling update from APP');
            $rootScope.$emit("updatequestions", {});
        } else {
            $location.path('/app/questions')
        };
    };


});
inqapp.controller('loginCtrl', function ($scope, $stateParams, $interval, $ionicSideMenuDelegate, MyServices, $location, $ionicPopup, $ionicLoading, $filter, $rootScope) {

    /*CONFIGURATONS*/
    $ionicSideMenuDelegate.canDragContent(false);

    var devicereadyviewanalyse = function () {
        MyServices.viewanalyse("Login Page");
    };
    document.addEventListener("deviceready", devicereadyviewanalyse, false);

    var showhint = true;
    $scope.showtooltip = false;
    //    $scope.user = {};
    //    $scope.user.promocode = "";

    $scope.$on('$ionicView.beforeEnter', function () {

        $location.replace();



        /*INITIALIZATIONS*/
        $scope.logindata = {
            'contact': 9820840950,
            'password': '1234'
        };

        console.log($scope.logindata);

        $scope.registerdata = {
            'name': '',
            'school': '',
            'standard': '',
            'board': '',
            'contact': '',
            'email': '',
            'password': '',
            'confirm': '',
            'promocode': ''
        };



        $scope.loginerror = false;
        $scope.registererror = false;
    });

    /*PASSWORD HINT*/
    $scope.showpasshint = function () {
        /*if (showhint) {
            $scope.showtooltip = true;
            showhint = false;
        };*/
        if ($scope.registerdata.password == '') {
            $scope.showtooltip = true;
        };
    };
    $scope.writingpassword = function () {
        $scope.showtooltip = false;
    };

    /*GET STANDARDS INITIAL*/
    var getstandardssuccess = function (data, status) {
        console.log(data);
        $scope.standards = data;
    };
    var getstandardserror = function (data, status) {
        $scope.standards = data;
    };
    MyServices.getstandards().success(getstandardssuccess).error(getstandardserror);

    /*GET BOARD INITIAL*/
    var getboardsuccess = function (data, status) {
        console.log(data);
        $scope.boards = data;
    };
    var getboarderror = function (data, status) {
        console.log(data);
        $scope.boards = data;
    };
    MyServices.getboards().success(getboardsuccess).error(getboarderror);

    /*ROUTING*/
    $scope.gotoregistertab = function () {

        $scope.loginerror = false;
        $scope.registererror = false;

        $interval(function () {
            $('select').material_select();
            console.log($('.select-wrapper .caret'));
            $('.select-wrapper .caret').empty();
            $('.select-wrapper .caret').addClass('icon-caret-down');
        }, 100, 1);

    };
    $scope.gotologintab = function () {
        $scope.loginerror = false;
        $scope.registererror = false;
    };

    /*VIEW LOGIC*/


    /*FUNCTIONS*/

    /*DO LOGIN*/
    var loginsuccess = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        if (data == "0") {
            $scope.loginerror = true;
        } else {
            $.jStorage.set("user", data);
            $scope.user = data;

            /*STORE DEVICE ID*/
            window.plugins.OneSignal.getIds(function (ids) {

                var adddevicesucsess = function (data, status) {
                    if (data == "1") {
                        MyServices.showToast('Device registered for notification.');
                    } else {
                        MyServices.showToast('Device not registered for notification.');
                    };
                };
                var adddeviceerror = function (data, status) {
                    MyServices.showToast('There might be a problem with your Internet Connection');
                };
                MyServices.adddevice($scope.user.userid, JSON.stringify(ids['userId'])).sucsess(adddevicesucsess).error(adddeviceerror);

            });



            if ($scope.user.userstype == 1) {
                $location.path('/app/standard');
            } else {
                $location.path('/app/questions');
            };
        };
    };
    var loginerror = function (data, status) {
        $ionicLoading.hide();
        MyServices.showToast('There might be a problem with your Internet Connection');
        console.log(data);
        $scope.interneterror = true;
    };
    $scope.dologin = function () {
        if ($scope.logindata.contact != '' && $scope.logindata.password != '') {
            $ionicLoading.show({
                template: '<ion-spinner class="android"></ion-spinner> <br>Logging In'
            });
            $scope.loginerror = false;
            MyServices.loginget($scope.logindata.contact, $scope.logindata.password).success(loginsuccess).error(loginerror);
        } else {
            MyServices.showToast('Please enter Phone Number and Password');
        };

    };

    /*DO REGISTER*/
    var checkcontactexistsuccess = function (data, status) {
        //        if (data == "0") {
        //            $scope.registererror = true;
        //            $scope.registererrormsg = "Phone Number already exists";
        //        } else {
        //            tempuser = data;
        //            $scope.user = data;
        //            $.jStorage.set("user", data);
        //            if ($scope.user.usertype == 1) {
        //                $location.path('/app/standard');
        //            } else {
        //                $location.path('/app/questions');
        //            };
        //        };
        console.log(typeof data);
        if (data == "1") {
            $scope.registererror = true;
            $scope.registererrormsg = "Phone Number already exists";
        } else {
            $rootScope.registrationdata = $scope.registerdata;
            $location.path('/app/otp');
        }
    };
    var checkcontactexisterror = function (data, status) {
        console.log(data);
        $scope.interneterror = true;
    };
    $scope.doregister = function () {
        console.log($scope.registerdata);
        if ($scope.registerdata.contact != '' && $scope.registerdata.password != '' && $scope.registerdata.email != '' && $scope.registerdata.standard != '') {
            console.log($scope.registerdata);
            if ($scope.registerdata.password == $scope.registerdata.confirm) {
                if (/^\d+$/.test($scope.registerdata.password)) {
                    $scope.registererror = false;
                    //                  
                    //                    $location.path('/otp');
                    MyServices.checkcontactexist($scope.registerdata.contact).success(checkcontactexistsuccess).error(checkcontactexisterror);
                } else {
                    $scope.registererror = true;
                    $scope.registererrormsg = "Passwords can be only 4 digit number";
                }
            } else {
                $scope.registererror = true;
                $scope.registererrormsg = "Passwords do not match";
            };
        } else {
            MyServices.showToast('Please enter all fields');
        };


    };


    /*FORGOT PASSWORD*/
    // An elaborate, custom popup

    $scope.forgotpassword = {
        'contact': ''
    };

    $scope.forgotpassmodal = function () {
        passpopup = $ionicPopup.show({
            templateUrl: 'templates/forgotpassword.html',
            cssClass: 'passpopup',
            scope: $scope
        });
    };


    var sendsmssuccess = function (data, status) {
        if (data.indexOf("successfully") != -1) {
            MyServices.showToast("SMS sent successfully");
        } else {
            MyServices.showToast("Could not sms password. Try Again");
        };
    };
    var sendsmserror = function (data, status) {
        MyServices.showToast("Could not sms password. Try Again");
    };

    var forgotpasswordsuccess = function (data, status) {
        $ionicLoading.hide();
        if (data == '1') {

            //Send new password in SMS
            var message = "Dear INQ User -" + $scope.forgotpassword.contact + ", Please find your temporary password " + $scope.val + ".Request you to change it from your profile page after logging in."

            MyServices.sendsms($scope.forgotpassword.contact, message).success(sendsmssuccess).error(sendsmserror);


            /*PASSWORD CHANGED SUCCESSFULLY*/
            var completePopup = $ionicPopup.alert({
                title: 'Password Updated',
                template: 'A new password has been sent to you by SMS.',
                cssClass: 'confirmpopup'
            });
            completePopup.then(function (res) {
                passpopup.close();
            });

        } else {
            $scope.forgotpassword = {
                'contact': ''
            };
            /*ERROR COULD NOT CHANGE PASSWORD*/
            MyServices.showToast("Could not change password. Try Again");
        };
    };

    var forgotpassworderror = function (data, status) {
        $ionicLoading.hide();
        /*ERROR INTERNET*/
        MyServices.showToast("You seem to have some problem in your Internet connection");
    };

    $scope.forgotpass = function () {
        if ($scope.forgotpassword.contact != '') {
            // A confirm dialog
            var confirmPopup = $ionicPopup.confirm({
                title: 'Request New Password',
                template: 'Are you sure you want to change your password ?',
                cssClass: 'confirmpopup'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    $ionicLoading.show({
                        template: '<ion-spinner class="android"></ion-spinner> <br>You will recieve an sms with your new password'
                    });

                    //Make new password
                    $scope.val = Math.floor(1000 + Math.random() * 9000);

                    MyServices.forgotpassword($scope.forgotpassword.contact, $scope.val).success(forgotpasswordsuccess).error(forgotpassworderror);

                } else {
                    console.log('You are not sure');
                }
            });
        } else {
            /*ERROR CURRENT PASSWORD IS WRONG*/
            MyServices.showToast("Please enter your phone number");
        };

    };
    $scope.hidepasswordpopup = function () {
        passpopup.close();
    };

    /*Coupon Code Pop-up*/


    checkcouponcodesuccess = function (response, status) {
        console.log(response);
        if (response == 'true') {
            promocodepopup.close();
            console.log('CORRECT CODE');
            $scope.registerdata.promocode = $scope.data.model;


        } else {
            console.log(response);

            $scope.errormessage = response;
            //            angular.element(document.querySelector('#errormessage')).html("Incorrect code, please try again");

        }



    }

    checkcouponcodeerror = function (response, status) {

        console.log("There seems to be a problem with the internet");

    }

    var promocodepopup;
    // When button is clicked, the popup will be shown...
    $scope.showPopup = function () {
        $scope.data = {}
        $scope.errormessage = '';
        // Custom popup
        promocodepopup = $ionicPopup.show({
            template: '<input type = "text" ng-model = "data.model" placeholder="Enter your promo code here"><p ng-bind="errormessage" class="promo-error"></p>',
            title: 'Enter the Promo Code <br><br><br> <img src="img/discount.png" class="promodiscountimage"><div class="promo-line"></div>',
            cssClass: 'promopopupwrapper',
            scope: $scope,
            buttons: [
                {
                    text: 'Cancel'
                }, {
                    text: '<b>Verify</b>',
                    type: 'button-positive',
                    onTap: function (e) {
                        e.preventDefault();
                        if (!$scope.data.model) {
                            //don't allow the user to close unless he enters model...
                            $scope.errormessage = "Promo code field should not be empty !";

                        } else {

                            //                            var deviceid = $filter('getdeviceid')();
                            var deviceid = '1fe6bbf7-5334-465e-a04b-4fb4aa3919c4';
                            MyServices.checkcouponcode(deviceid, $scope.data.model.toUpperCase()).success(checkcouponcodesuccess).error(checkcouponcodeerror);
                        }
                    }
            }
         ]

        });

        /*promocodepopup.then(function (res) {

            console.log('Tapped!', res);
        });*/
    };


});







inqapp.controller('otpCtrl', function ($scope, $stateParams, $rootScope, MyServices, $location) {
    $scope.otp = {};
    $scope.otp.value;
    var generatedotp;
    console.log($rootScope.registrationdata);



    /*CALLBACKS*/
    registersuccess = function (data, response) {
        tempuser = data;
        $scope.user = data;
        console.log(data);
        $.jStorage.set("user", data);
        if ($scope.user.userstype == 1) {
            $location.path('/app/standard');
        } else {
            $location.path('/app/questions');
        };
    };
    registererror = function (data, response) {

    };

    sendsmssuccess = function (response, status) {

    };
    sendsmserror = function (error, status) {

    };
    var sendotp = function () {
        var min = 1111,
            max = 9999;
        generatedotp = Math.floor(Math.random() * (max - min + 1)) + min;
        message = "Hey, use "+generatedotp+" as the OTP for registering into Sunalis Classes.";
        MyServices.sendsms($rootScope.registrationdata.contact, message).success(sendsmssuccess).error(sendsmserror);
        console.log(generatedotp);
    };

    sendotp();
    $scope.verifiyotp = function () {

        if (generatedotp == parseInt($scope.otp.value)) {
            console.log('correct otp ! continue the things ');
            MyServices.register($rootScope.registrationdata).success(registersuccess).error(registererror);
        } else {
            console.log('The otp is incorrect  !');
        }


    };


});
inqapp.controller('PlaylistsCtrl', function ($scope, $interval, MyServices, $location, $rootScope, $ionicLoading, $ionicSideMenuDelegate, $ionicScrollDelegate, $ionicPopup, $cordovaSocialSharing) {


    /*CONFIGURATIONS*/
    $ionicSideMenuDelegate.canDragContent(false);

    /*INITIALIZATIONS*/
    $scope.search = '';

    /*INITIALIZATION CALLS*/
    $scope.filterparameters = angular.copy(filterparameters);


    /*SUBJECTS GET*/
    var getusersubjectssuccess = function (data, status) {
        console.log(data);
        $scope.subjects = data;
        for (var sd = 0; sd < $scope.subjects.length; sd++) {
            $scope.subjects[sd].isactive = true;
            for (var cd = 0; cd < $scope.subjects[sd].chapters.length; cd++) {
                $scope.subjects[sd].chapters[cd].selected = false;
            };
        };

        if ($.jStorage.get("user").userstype > 1) {
            $.jStorage.set("bottombar", data);
        };
        console.log($scope.subjects);
    };
    var getusersubjectserror = function (data, status) {
        console.log(data);
    };

    /* GET TRENDING QUESTIONS */
    $scope.trendingquestions = [];
    var gettrendingquestionssuccess = function (data, status) {
        console.log(data);
        $scope.trendingquestions = data;
    };
    var gettrendingquestionserror = function (data, status) {
        console.log(data);
    };
    MyServices.gettrendingquestions($.jStorage.get("user").standardid, $.jStorage.get("user").userid).success(gettrendingquestionssuccess).error(gettrendingquestionserror);

    /*GET QUESTIONS*/
    var getquestionssuccess = function (data, status) {


        $ionicLoading.hide();

        $scope.questions = [];
        console.log(data);
        $scope.questions = data;



        $interval(function () {
            $ionicScrollDelegate.scrollTo(0, offsetvalue, true)
            offsetvalue = 0;
        }, 20, 1);


        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');

    };

    var getquestionserror = function (data, status) {
        console.log(data);
        /*INTERNET ERROR*/
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>There might be a problem with your Internet Connection'
        });
        $ionicLoading.hide();
    };

    $scope.searchquestions = function (s) {

        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br> Fetching Questions'
        });

        $scope.questions = [];

        console.log(filterparameters);

        if (quesfilterapplied) {

            $scope.quesfilterapplied = true;
            $scope.showclearfilter = true;
            MyServices.getquestions(filterparameters, s).success(getquestionssuccess).error(getquestionserror);
        } else {
            filterparameters = {
                'userid': $scope.user.userid,
                'standardid': $scope.user.standardid,
                'subjectid': '',
                'chapters': '',
                'number': 0,
                'limit': 30,
                'filters': ''
            };
            MyServices.getquestions(filterparameters, s).success(getquestionssuccess).error(getquestionserror);
        };
    };
    $rootScope.$on("clearfilters", function () {
        $scope.clearfilter();
    });

    $scope.showclearfilter = false;
    $scope.clearfilter = function () {

        console.log("clearing filter");

        $scope.showchapters = false;
        $scope.filterapplied = false;
        $scope.selectedsubject = null;
        $scope.selectedsubjectid = null;
        $scope.dummychapterprefix = "";
        $scope.allchapsbutton = true;

        for (var cc = 0; cc < $scope.chapters.length; cc++) {
            if ($scope.chapters[cc].selected) {
                $scope.chapters[cc].selected = false;
            };
        };

        $scope.chapters = [];

        $scope.questions = [];
        quesfilterapplied = false;
        $scope.quesfilterapplied = false;
        filterparameters = {
            'userid': $scope.user.userid,
            'standardid': $scope.user.standardid,
            'subjectid': '',
            'chapters': '',
            'number': 0,
            'limit': 20,
            'filters': ''
        };
        $scope.showchapters = false;
        $scope.showclearfilter = false;
        MyServices.getquestions(filterparameters).success(getquestionssuccess).error(getquestionserror);
    };

    $rootScope.$on("updatequestions", function () {
        $scope.updatequestions();
    });

    $scope.updatequestions = function () {

        //filterparameters.number = 0;
        //filterparameters.limit = 30;
        $scope.lmbdisabled = false;

        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br> Fetching Questions'
        });

        $scope.questions = [];

        console.log(filterparameters);

        if (quesfilterapplied) {

            $scope.quesfilterapplied = true;
            $scope.showclearfilter = true;
            MyServices.getquestions(filterparameters).success(getquestionssuccess).error(getquestionserror);
        } else {
            filterparameters = {
                'userid': $scope.user.userid,
                'standardid': $scope.user.standardid,
                'subjectid': '',
                'chapters': '',
                'number': 0,
                'limit': 30,
                'filters': ''
            };
            MyServices.getquestions(filterparameters).success(getquestionssuccess).error(getquestionserror);
        };
    };

    var getloadmoresuccess = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        for (var pq = 0; pq < data.length; pq++) {
            $scope.questions.push(data[pq]);
        };
        if (data.length < 10) {
            $scope.lmbdisabled = true;
        };
    };
    var getloadmoreerror = function (data, status) {
        $ionicLoading.hide();
        /*INTERNET ERROR*/
    };
    $scope.loadmore = function () {
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Fetching Questions'
        });
        filterparameters.number = $scope.questions.length;
        filterparameters.limit = 10;
        console.log(filterparameters);
        MyServices.getquestions(filterparameters, $scope.search).success(getloadmoresuccess).error(getloadmoreerror);
    };

    var devicereadyviewanalyse = function () {
        MyServices.viewanalyse("Feed Page");
    };
    document.addEventListener("deviceready", devicereadyviewanalyse, false);

    /*ON ENTER PLAYLISTS*/
    $scope.$on('$ionicView.beforeEnter', function () {
        // Code you want executed every time view is opened
        $scope.user = $.jStorage.get("user");

        MyServices.useranalyse($scope.user.userid);

        $scope.quesfilterapplied = false;

        $scope.usertypes = usertypes;

        if ($.jStorage.get("bottombar") == null || $.jStorage.get("bottombar") == []) {
            console.log("no bottom bar");
            //$scope.subjects = tempusersubjects;
            MyServices.getusersubjects($scope.user.standardid).success(getusersubjectssuccess).error(getusersubjectserror);
        } else {
            $scope.subjects = $.jStorage.get("bottombar");
            //$scope.subjects = tempusersubjects;
        };

        $scope.questions = [];
        console.log('calling update from ENTER');
        $scope.updatequestions();

    });


    var checkfornewmessagessuccess = function (data, status) {
        console.log(data);
        if (data > 0) {
            $scope.newmessages = true;
        } else {
            $scope.newmessages = false;
        };
    };
    var checkfornewmessageserror = function (data, status) {

    };
    var checkfornew = function () {
        MyServices.checkfornewmessages().success(checkfornewmessagessuccess).error(checkfornewmessageserror);
    };

    var refreshmsg;

    $scope.$on("$ionicView.enter", function (scopes, states) {
        // do whatever
        $scope.newmessages = false;

        refreshmsg = $interval(function () {
            checkfornew();
        }, 15000);
    });

    /*CANCEL ON LEAVE*/
    $scope.$on('$ionicView.beforeLeave', function () {
        console.log('leaving');
        $interval.cancel(refreshmsg);
    });
    /*----*/

    /*STATES*/

    /*SUBJECT QUESTIONS FILTER*/

    $scope.showchapters = false;
    $scope.filterapplied = false;
    $scope.selectedsubject = null;
    $scope.selectedsubjectid = null;
    $scope.dummychapterprefix = "";
    $scope.allchapsbutton = true;

    $scope.chapters = [];

    $scope.selectallchaps = function () {
        for (var c = 0; c < $scope.chapters.length; c++) {
            $scope.chapters[c].selected = true;
        };
        $scope.allchapsbutton = false;
    };

    $scope.addtochapfilter = function () {
        $scope.allchapsbutton = false;
        for (var c = 0; c < $scope.chapters.length; c++) {
            if ($scope.chapters[c].selected == false) {
                $scope.allchapsbutton = true;
                break;
            } else {
                $scope.allchapsbutton = false;
            };
        };
    };

    $scope.addfilters = function () {
        $scope.showchapters = false;

        $scope.chapfilter = [];

        for (var cc = 0; cc < $scope.chapters.length; cc++) {
            if ($scope.chapters[cc].selected) {
                $scope.chapfilter.push($scope.chapters[cc].chapterid);
            };
        };
        console.log($scope.chapfilter);

        $scope.questions = [];
        console.log($scope.selectedsubjectid);

        filterparameters.userid = $scope.user.userid
        filterparameters.standardid = $scope.user.standardid
        filterparameters.subjectid = $scope.selectedsubjectid
        filterparameters.chapters = $scope.chapfilter
        filterparameters.number = 0
        filterparameters.limit = 20

        quesfilterapplied = true;
        $scope.quesfilterapplied = true;
        MyServices.getquestions(filterparameters).success(getquestionssuccess).error(getquestionserror);
    };

    $scope.selectsubject = function (index, subid) {

        $scope.allchapsbutton = true;

        if ($scope.showchapters) {
            if ($scope.selectedsubject == index) {
                $scope.showchapters = false;
                $scope.selectedsubject = null;
                $scope.selectedsubjectid = null;
            } else {
                $scope.chapters = $scope.subjects[index].chapters;
                for (var c = 0; c < $scope.chapters.length; c++) {
                    $scope.chapters[c].selected = false;
                };
                $scope.selectedsubject = index;
                $scope.selectedsubjectid = subid;
            };
        } else {
            $scope.showchapters = true;

            $scope.chapters = $scope.subjects[index].chapters;
            console.log($scope.chapters);
            $scope.selectedsubject = index;
            $scope.selectedsubjectid = subid;
        };



        $scope.subjects[index].isactive = true;
    };

    /*LIKE A QUESTION FROM LISTING PAGE*/
    $scope.starquestion = function (qid, index) {

        var starquestionsuccess = function (data, status) {
            console.log(data);
            if (data != '1') {
                $scope.questions[index].starcount += $scope.questions[index].starred == '1' ? -1 : 1;
                $scope.questions[index].starred = $scope.questions[index].starred == '1' ? 0 : 1;
                MyServices.showToast("Could not thank question.");
            };
        };

        var starquestionerror = function (data, status) {
            console.log(data);
            $scope.questions[index].starcount += $scope.questions[index].starred == '1' ? -1 : 1;
            $scope.questions[index].starred = $scope.questions[index].starred == '1' ? 0 : 1;
            MyServices.showToast("Could not thank question.");
        };



        $scope.questions[index].starcount += $scope.questions[index].starred == '1' ? -1 : 1;
        $scope.questions[index].starred = $scope.questions[index].starred == '1' ? 0 : 1;

        if ($scope.questions[index].starred == '1') {
            $('.like-icon-' + index).addClass('like-animation');
            setTimeout(function () {
                $('.like-icon-' + index).removeClass('like-animation');
            }, 150);
        };

        console.log($scope.questions[index].starcount);

        MyServices.starquestion($scope.user.userid, qid).success(starquestionsuccess).error(starquestionerror);
    };

    /* SHARE QUESTION IN PLAYLISTS */
    $scope.sharequestion = function (qid) {
        $cordovaSocialSharing.share('Check out this question on Sunalis Classes App', 'Sunalis Classes Question', null, 'http://www.sunalisclasses.com/sharedoubt.php?question=' + qid);
    };


    var bind;
    var bq;
    var bookmarkquessuccess = function (data, status) {
        console.log(data);
        if (data == 0) {
            MyServices.showToast("Could not bookmark question");
        } else {
            if ($scope.questions[bind].bookmarked == 0) {
                $scope.questions[bind].bookmarked = 1;
            } else {
                $scope.questions[bind].bookmarked = 0;
            };
        }
    };
    var bookmarkqueserror = function (data, status) {
        console.log(data);
    };
    $scope.bookmarkques = function (qid) {
        console.log(qid);
        for (var bk = 0; bk < $scope.questions.length; bk++) {
            if ($scope.questions[bk].questionid == qid) {
                bind = bk;
                break;
            };
        };
        bq = qid;
        MyServices.bookmarkques($scope.user.userid, qid).success(bookmarkquessuccess).error(bookmarkqueserror);
    };
    $scope.gotochat = function () {
        $location.path("/app/chatlist");
    };
    $scope.gotofilter = function () {

        $('#floatbtn').closeFAB();
        $scope.floatactive = false;

        $location.path("/app/filter");
    };

    /*CALLBACK FUNCTIONS*/

    /*checkpaidquestionssuccess = function (response, status) {
        if (response) {
            $rootScope.paidquestion = response.data;
            var confirmPopup = $ionicPopup.confirm({
                title: 'Questions left',
                template: "Your daily questions are finished. Do you want to use your premium question ?"
            });

            confirmPopup.then(function (res) {
                if (res) {
                    $location.path("/app/ask");
                } else {
                    console.log('You are not sure');
                }
            });

        } else {
            MyServices.showToast("You cannot ask anymore Questions Today. Wait for tomorrow.");
        }
    };

    checkpaidquestionserror = function (error, status) {

    };*/



    $scope.gotoask = function () {

        $('#floatbtn').closeFAB();
        $scope.floatactive = false;

        if ($scope.user.userstype == 3 && $scope.questionsleft <= 0) {
            //MyServices.showToast("You cannot ask anymore Questions Today. Wait for tomorrow.");
            /*CHECK IF HAS PREMIUM QUESTIONS OR NOT (ASK POPUP IF USER WANTS TO USE PREMIUM QUESTION OR NOT)*/
            //            MyServices.checkpaidquestions($scope.user.userid).success(checkpaidquestionssuccess).error(checkpaidquestionserror);
            if ($rootScope.paidquestions > 0) {
                MyServices.confirmationpopupforpaidquestions();

            } else {
                MyServices.showToast("You cannot ask anymore Questions Today. Wait for tomorrow.");
            }

        } else {
            $location.path("/app/ask");
        };

    };


    $scope.gotoanswers = function (id, ind) {
        $('.youtubevideo').each(function () {
            $(this).attr('src', $('iframe').attr('src'));
        });
        if ($scope.questions[ind].premium == 0 || $scope.user.userstype != '3') {
            offsetvalue = -1 * $('.questionslist').offset().top;
            $location.path("/app/questions/:" + id);
        } else {
            var premiumPopup = $ionicPopup.alert({
                title: 'Exclusive Content',
                template: 'You need to register at Sunalis Classes to access Exclusive Content',
                cssClass: 'confirmpopup'
            });
        };
    };

    $scope.showtrending = false;
    $scope.gototrending = function () {
        console.log("TREND");
        $scope.showtrending = !$scope.showtrending;
    };

    $scope.rotatenow = false;
    /*FLOAT BUTTON*/
    $scope.checkoverlay = function () {
        $interval(function () {
            console.log($('#floatbtn').hasClass('active'));
            $scope.floatactive = $('#floatbtn').hasClass('active');
        }, 200, 1);
    };


});

inqapp.controller('PlaylistCtrl', function ($scope, $stateParams, MyServices, $location, $ionicPopup, $ionicModal, $rootScope, $ionicLoading, $ionicHistory, $filter, $cordovaVibration, $cordovaSocialSharing) {
    $scope.usertypes = usertypes;

    var getquestiondatasuccess = function (data, status) {
        console.log(data);
        $scope.question = data;

        /* Give COntent Type Area Margin */
        if ($scope.question.solutionviewed == 0) {
            $scope.contentbottommargin = {
                'margin-bottom': "42px"
            };
        } else {
            $scope.contentbottommargin = {
                'margin-bottom': "0px"
            };
        };
    };

    var getquestiondataerror = function (data, status) {
        console.log(data);
        /*INTERNET ERROR*/
    };

    var getanswerssuccess = function (data, status) {
        $scope.answers = [];
        $ionicLoading.hide();
        console.log(data);
        $scope.answers = data;

        if (answerposted == true) {
            answerposted = false;
            $('.answersscroll').animate({
                scrollTop: $('.answersscroll')[0].scrollHeight
            }, 1000);
        };

        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');

    };

    var getanswerserror = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        /*INTERNET ERROR*/
    };

    /*PULL TO REFRESH ANSWERS*/
    $scope.updateanswers = function () {
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Fetching Answers'
        });

        /*FETCH ANSWERS*/
        MyServices.getanswers($scope.questionid).success(getanswerssuccess).error(getanswerserror);
    };

    var answerposted = false;

    $scope.$on('$ionicView.beforeEnter', function () {
        // Code you want executed every time view is opened
        $scope.user = $.jStorage.get("user");

        MyServices.useranalyse($scope.user.userid);

        $scope.questionid = $stateParams.id.substr(1);

        $scope.showimg = false;


        var pictureSource; // picture source
        var destinationType; // sets the format of returned value

        document.addEventListener("deviceready", onDeviceReady, false);

        $scope.answerdata = {
            'answer': '',
            'image': null,
            'questionid': $stateParams.id.substr(1)
        };



        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Fetching Answers'
        });

        /*FETCH QUESTION DETAILS*/
        MyServices.getquestiondata($scope.questionid).success(getquestiondatasuccess).error(getquestiondataerror);
        /*FETCH ANSWERS*/
        MyServices.getanswers($scope.questionid).success(getanswerssuccess).error(getanswerserror);
    });


    /*FUNCTION*/

    /*Count Of Other User Answers*/
    $scope.haveothersanswered = function () {
        var othersanswerd = false;
        for (var oac = 0; oac < $scope.answers.length; oac++) {
            if ($scope.answers[oac].auserid != $scope.user.userid) {
                othersanswerd = true;
            };
        };
        return othersanswerd;
    };

    /*SEND ANSWERS*/
    var answerquessuccess = function (data, status) {
        $ionicLoading.hide();
        $scope.answerdata = {
            'answer': '',
            'image': null,
            'questionid': $scope.questionid
        };
        $scope.showimg = false;

        $('#answer').css('height', '30px');
        answerposted = true;
        $scope.question.answerscount++;
        $scope.question.answered = '1';
        console.log(data);
        if (data == "1") {

            // Vibrate 100ms
            //$cordovaVibration.vibrate(100);

            MyServices.getanswers($scope.questionid).success(getanswerssuccess).error(getanswerserror);

            $ionicLoading.show({
                template: '<ion-spinner class="android"></ion-spinner> <br>Fetching Answers'
            });

        } else {
            /*ERROR*/
            MyServices.showToast("Could not post Answer");
        };
    };
    var answerqueserror = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        /*INTERNET ERROR*/
    };

    var viewedsolutionsuccess = function (data, status) {
        console.log(data);
        $scope.question.solutionviewed = 1;
        $scope.contentbottommargin = {
            'margin-bottom': "0px"
        };
        $ionicLoading.hide();
    };
    var viewedsolutionerror = function (data, status) {
        console.log(data);
        $ionicLoading.hide();
    };
    $scope.viewsolution = function () {
        MyServices.viewedsolution($scope.questionid).success(viewedsolutionsuccess).error(viewedsolutionerror);
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Fetching Answers'
        });
    };


    /*UPLOAD IMAGE*/
    $scope.imageuploaded = false;
    $scope.fileURI = '';


    $scope.filetransferdone = function (data) {

        console.log("SUCCESS");
        console.log(data);
        $scope.answerdata.image = data.response;

        console.log($scope.answerdata);
        MyServices.answerques($scope.user.userid, $scope.answerdata.answer, $scope.questionid, $scope.answerdata.image).success(answerquessuccess).error(answerqueserror);
    };

    $scope.filetransfer = function () {

        var win = function (data, status) {
            $scope.fileURI = '';
            $ionicLoading.hide();
            $ionicLoading.hide();

            $scope.filetransferdone(data);

        };
        var fail = function (error) {
            $ionicLoading.hide();
            console.log(error);
            MyServices.showToast('There seems to be a problem with the internet');
        };

        console.log($scope.fileURI);
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Uploading Image...'
        });

        $scope.imgname = $scope.fileURI.substr($scope.fileURI.lastIndexOf('/') + 1);

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = $scope.imgname;
        options.mimeType = "image/jpeg";

        var params = {};
        params.value1 = "test";
        params.value2 = "param";

        options.params = params;
        options.chunkedMode = false;

        console.log(options);

        /*if ($scope.fileURI.lastIndexOf('?') > 0) {
            $scope.fileURI.substr(0, $scope.fileURI.lastIndexOf('?'));
        };*/

        var ft = new FileTransfer();
        ft.upload($scope.fileURI, encodeURI("http://learnwithinq.com/appfilesv2/uploadimage.php"), win, fail, options, true);
    };


    $scope.sendanswer = function () {

        if ($scope.imageuploaded == true) {
            if ($scope.answerdata.answer != '') {

                /*CALL TO OTHER FUNCTION*/
                $scope.filetransfer();

            } else {
                /*ERROR write answer*/
                MyServices.showToast("Type an answer before send");
            };
        } else {
            if ($scope.answerdata.answer != '') {
                console.log($scope.answerdata);
                MyServices.answerques($scope.user.userid, $scope.answerdata.answer, $scope.questionid, $scope.answerdata.image).success(answerquessuccess).error(answerqueserror);
            } else {
                /*ERROR write answer*/
                MyServices.showToast("Type an answer before send");
            };
        };

    };
    $scope.showimg = false;

    var pictureSource; // picture source
    var destinationType; // sets the format of returned value

    document.addEventListener("deviceready", onDeviceReady, false);

    function onDeviceReady() {
        pictureSource = navigator.camera.PictureSourceType;
        destinationType = navigator.camera.DestinationType;

        MyServices.viewanalyse("Answers Page");
    }

    function clearCache() {
        navigator.camera.cleanup();
    }

    var retries = 0;

    var onCapturePhoto = function (fileURI) {
        console.log(fileURI);
        $scope.fileURI = fileURI;

        /*CALL TO OTHER FUNCTION*/
        $scope.imageuploaded = true;

        $scope.showimg = true;
        var image = document.getElementById('myanswerImage');
        console.log(image);
        if (image != null) {
            image.src = $scope.fileURI;

            $scope.$apply();
        };

        clearCache();

    };
    var onFail = function (message) {
        alert('Failed because: ' + message);
    };


    $scope.capturephoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            allowEdit: true,
            correctOrientation: true
        });
    };
    $scope.pickphoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            correctOrientation: true
        });
    };

    /*DOWNLOAD IMAGE*/
    $scope.downloadimage = function () {
        console.log("downloading image");
        var fileTransfer = new FileTransfer();
        var image = $filter('imagepath')($scope.imageSrc);
        var uri = encodeURI(image);
        var fileURL = cordova.file.externalRootDirectory + 'Download/' + $scope.imageSrc;

        fileTransfer.download(
            uri,
            fileURL,
            function (entry) {

                window.MediaScannerPlugin.scanFile(
                    function (msg) {
                        console.log(msg);
                    },
                    function (err) {
                        console.log(err);
                    }
                );

                var completePopup = $ionicPopup.alert({
                    title: 'Image Downloaded',
                    template: 'This image has been downloaded to your device memory',
                    cssClass: 'confirmpopup'
                });
                console.log("download complete: " + entry.toURL());
            },
            function (error) {
                console.log("download error source " + error.source);
                console.log("download error target " + error.target);
                console.log("download error code" + error.code);
                var completePopup = $ionicPopup.alert({
                    title: 'Image Not Downloaded',
                    template: 'This image could not be downloaded.Try Again.',
                    cssClass: 'confirmpopup'
                });
            },
            false, {
                headers: {
                    "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                }
            }
        );
    };

    /*VERIFY ANSWER*/
    var va;
    var aind;
    var verifyanswersuccess = function (data, status) {
        console.log(data);
        if (data == 1) {
            if ($scope.answers[aind].verified == 0) {
                $scope.answers[aind].verified = 1;
            } else {
                $scope.answers[aind].verified = 0;
            };
        } else {
            /*VERIFY ANSWER ERROR*/
            MyServices.showToast("Could not verify Answer. Try Again.");
        };
    };
    var verifyanswererror = function (data, status) {
        console.log(data);
        /*INTERNET ERROR*/
    };
    $scope.verifyanswer = function (aid) {
        if ($scope.user.userstype == '1') {
            for (var vf = 0; vf < $scope.answers.length; vf++) {
                if ($scope.answers[vf].answerid == aid) {
                    aind = vf;
                    break;
                };
            };
            va = aid;
            MyServices.verifyanswer($scope.user.userid, aid).success(verifyanswersuccess).error(verifyanswererror);
        };
    };

    /*VERIFY QUESTION*/
    var va;
    var aind;
    var verifyquestionsuccess = function (data, status) {
        console.log(data);
        if (data == 1) {
            if ($scope.question.verified == 0) {
                $scope.question.verified = 1;
            } else {
                $scope.question.verified = 0;
            };
        } else {
            /*VERIFY ANSWER ERROR*/
            MyServices.showToast("Could not verify Question. Try Again.");
        };
    };
    var verifyquestionerror = function (data, status) {
        console.log(data);
        /*INTERNET ERROR*/
    };
    $scope.verifyquestion = function () {
        if ($scope.user.userstype == '1') {
            if ($scope.question.verified == '0') {
                MyServices.verifyquestion($scope.user.userid, $scope.questionid).success(verifyquestionsuccess).error(verifyquestionerror);
            } else {
                MyServices.showToast("Question already verified.");
            };
        };
    };

    /* STAR QUESTION FROM QUESTION PAGE*/
    var starquestionsuccess = function (data, status) {
        console.log(data);
        if (data != '1') {
            $scope.question.starcount += $scope.question.star == '1' ? -1 : 1;
            $scope.question.star = $scope.question.star == '1' ? 0 : 1;
            MyServices.showToast("Could not thank question.");
        };
    };
    var starquestionerror = function (data, status) {
        console.log(data);
        $scope.question.starcount += $scope.question.star == '1' ? -1 : 1;
        $scope.question.star = $scope.question.star == '1' ? 0 : 1;
        MyServices.showToast("Could not thank question.");
    };
    $scope.starquestion = function () {
        $scope.question.starcount += $scope.question.star == '1' ? -1 : 1;
        $scope.question.star = $scope.question.star == '1' ? 0 : 1;

        if ($scope.question.star == '1') {
            $('.question-like-icon').addClass('like-animation');
            setTimeout(function () {
                $('.question-like-icon').removeClass('like-animation');
            }, 150);
        };

        MyServices.starquestion($scope.user.userid, $scope.questionid).success(starquestionsuccess).error(starquestionerror);
    };

    /* STAR ANSWER*/
    $scope.staranswer = function (answerid, index) {
        var staranswersuccess = function (data, status) {
            console.log(data);
            if (data != '1') {
                $scope.answers[index].starcount += $scope.answers[index].star == '1' ? -1 : 1;
                $scope.answers[index].star = $scope.answers[index].star == '1' ? 0 : 1;
            };
        };
        var staranswererror = function (data, status) {
            console.log(data);
            $scope.answers[index].starcount += $scope.answers[index].star == '1' ? -1 : 1;
            $scope.answers[index].star = $scope.answers[index].star == '1' ? 0 : 1;
            MyServices.showToast("Could not thank question.");
        };

        $scope.answers[index].starcount += $scope.answers[index].star == '1' ? -1 : 1;
        $scope.answers[index].star = $scope.answers[index].star == '1' ? 0 : 1;

        if ($scope.answers[index].star == '1') {
            $('.answer-like-icon-' + index).addClass('like-animation');
            setTimeout(function () {
                $('.answer-like-icon-' + index).removeClass('like-animation');
            }, 150);
        };

        MyServices.staranswer($scope.user.userid, answerid).success(staranswersuccess).error(staranswererror);
    };

    /* OPEN VIEWS QUESTION USERS POPUP */
    $scope.questionviedusers = [];
    var getviewedquestionuserssuccess = function (data, status) {
        console.log(data);
        $scope.questionviedusers = data;
    };
    var getviewedquestionuserserror = function (data, status) {
        console.log(data);
        MyServices.showToast("Could not get viewed users.");
    };
    $scope.showviewedquestionusers = function (count) {
        if (count > 0) {
            MyServices.getviewedquestionusers($scope.questionid).success(getviewedquestionuserssuccess).error(getviewedquestionuserserror);
        };
        var viewedquestionusersPopup = $ionicPopup.show({
            template: '<div ng-repeat="au in questionviedusers" class="row star-question-users-wrapper"><div class="col col-20"><img src="{{usertypes[au.usertype - 1].image}}" width="100%" /></div><div class="col col-80"><h6>{{au.username}}</h6></div></div>',
            title: 'Users who viewed this question',
            scope: $scope,
            cssClass: 'advanced-filter-popup',
            buttons: [
                {
                    text: '<b>Done</b>',
                    type: 'button-positive',
                    onTap: function (e) {}
              }
            ]
        });
    };

    /* OPEN STAR QUESTION USERS POPUP */
    $scope.questionusers = [];
    var getstarquestionuserssuccess = function (data, status) {
        console.log(data);
        $scope.questionusers = data;
    };
    var getstarquestionuserserror = function (data, status) {
        console.log(data);
    };
    $scope.showstarquestionusers = function (count) {
        if (count > 0) {
            MyServices.getstarquestionusers($scope.questionid).success(getstarquestionuserssuccess).error(getstarquestionuserserror);
        };
        var starquestionusersPopup = $ionicPopup.show({
            template: '<div ng-repeat="au in questionusers" class="row star-question-users-wrapper"><div class="col col-20"><img src="{{usertypes[au.usertype - 1].image}}" width="100%" /></div><div class="col col-80"><h6>{{au.username}}</h6></div></div>',
            title: 'Users who starred this question',
            scope: $scope,
            cssClass: 'advanced-filter-popup',
            buttons: [
                {
                    text: '<b>Done</b>',
                    type: 'button-positive',
                    onTap: function (e) {}
              }
            ]
        });
    };



    /* OPEN STAR ANSWER USERS POPUP */
    $scope.answerusers = [];
    var getstaransweruserssuccess = function (data, status) {
        console.log(data);
        $scope.answerusers = data;
    };
    var getstaransweruserserror = function (data, status) {
        console.log(data);
    };
    $scope.showstaranswerusers = function (answerid, count) {
        if (count > 0) {
            MyServices.getstaranswerusers(answerid).success(getstaransweruserssuccess).error(getstaransweruserserror);
        };
        var staranswerusersPopup = $ionicPopup.show({
            template: '<div ng-repeat="au in answerusers" class="row star-question-users-wrapper"><div class="col col-20"><img src="{{usertypes[au.usertype - 1].image}}" width="100%" /></div><div class="col col-80"><h6>{{au.username}}</h6></div></div>',
            title: 'Users who thanked this answer',
            scope: $scope,
            cssClass: 'advanced-filter-popup',
            buttons: [
                {
                    text: '<b>Done</b>',
                    type: 'button-positive',
                    onTap: function (e) {}
              }
            ]
        });
    };

    /*SHARE QUESTION IN PLAYLIST */
    $scope.sharequestion = function () {
        console.log($scope.questionid);
        $cordovaSocialSharing.share('Check out this question on Sunalis Classes App', 'Sunalis Classes Question', null, 'http://www.sunalisclasses.com/sharedoubt.php?question=' + $scope.questionid);
    };

    /*OPEN IMAGE POPUP*/

    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.openModal = function () {
        $scope.modal.show();
    };

    $scope.closeModal = function () {
        $scope.modal.hide();
    };


    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function () {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
        // Execute action
    });
    $scope.$on('modal.shown', function () {
        console.log('Modal is shown!');
    });
    $scope.showImage = function (img) {
        console.log(img);
        $scope.imageSrc = img;
        $scope.openModal();
    };

    /*EDIT DELETE*/
    var deleteanswersuccess = function (data, status) {
        console.log(data);
        if (data == '1') {
            /*POPUP*/
            /*ON OK*/
            $scope.question.answerscount = $scope.question.answerscount - 1;
            MyServices.getanswers($scope.questionid).success(getanswerssuccess).error(getanswerserror);
        } else {
            /*ERROR COULD NOT DELETE*/
            MyServices.showToast("Could not delete Answer. Try Again.");
        };
    };
    var deleteanswererror = function (data, status) {
        console.log(data);
        /*INTERNET ERROR*/
    };
    $scope.deleteanswer = function (id) {
        // A confirm dialog
        var confirmPopup = $ionicPopup.confirm({
            title: 'Delete Answer',
            template: 'Are you sure you want to delete this answer ?',
            cssClass: 'confirmpopup'
        });

        confirmPopup.then(function (res) {
            if (res) {
                MyServices.deleteanswer(id).success(deleteanswersuccess).error(deleteanswererror);
            } else {
                console.log('You are not sure');
            }
        });

    };

    /*EDIT DELETE QUESTION*/
    var deletequestionsuccess = function (data, status) {
        console.log(data);
        if (data == '1') {
            /*POPUP*/
            /*ON OK*/
            $location.path('/app/questions');
        } else {
            /*ERROR COULD NOT DELETE*/
            MyServices.showToast("Could not delete Question. Try Again.");
        };
    };
    var deletequestionerror = function (data, status) {
        console.log(data);
        /*INTERNET ERROR*/
    };
    $scope.deletequestion = function () {
        // A confirm dialog
        var confirmPopup = $ionicPopup.confirm({
            title: 'Delete Question',
            template: 'Are you sure you want to delete this question ?',
            cssClass: 'confirmpopup'
        });

        confirmPopup.then(function (res) {
            if (res) {
                MyServices.deletequestion($scope.questionid).success(deletequestionsuccess).error(deletequestionerror);
            } else {
                console.log('You are not sure');
            }
        });

    };

    $scope.editanswer = function (id) {
        questionid = $scope.questionid;
        edittype = "answer";
        editid = id;
        $location.path('/app/edit');
    };
    $scope.editquestion = function () {
        edittype = "question";
        questionid = $scope.questionid;
        $location.path('/app/edit');
    };


    /*BOOKMARK*/
    var bind;
    var bq;
    var bookmarkquessuccess = function (data, status) {
        console.log(data);
        if (data == 0) {
            /*NOT BOOKMARKED ERROR*/
            MyServices.showToast("Could not bookmark question. Try Again");
        } else {
            if ($scope.question.bookmarked == 0) {
                $scope.question.bookmarked = 1;
            } else {
                $scope.question.bookmarked = 0;
            };
        }
    };
    var bookmarkqueserror = function (data, status) {
        console.log(data);
        /*INTERNET ERROR*/
    };
    $scope.bookmarkques = function (qid) {
        console.log(qid);
        bq = qid;
        MyServices.bookmarkques($scope.user.userid, qid).success(bookmarkquessuccess).error(bookmarkqueserror);
    };

    /*CLEAR CACHE*/
    $scope.$on('$ionicView.beforeLeave', function () {
        $ionicHistory.clearCache();
        $scope.showimg = false;
        $scope.imageuploaded = false;

    });

    $scope.goback = function () {
        $location.path("/app/questions");
    };


});
inqapp.controller('standardCtrl', function ($scope, $stateParams, $location, $ionicSideMenuDelegate, MyServices, $ionicLoading, $interval) {

    /*CONFIGURATIONS*/
    $ionicSideMenuDelegate.canDragContent(false);

    /*INITIALIZATIONS*/
    $scope.user = $.jStorage.get("user");
    $scope.tempuser = $.jStorage.get("user");

    var checkpending = function (n, nid) {
        MyServices.getpendingstatus(nid).success(function (data, status) {
            $scope.standards[n].pending = data;
        });
    };

    var getstandardssuccess = function (data, status) {
        $ionicLoading.hide();
        $scope.standards = data;
        for (var s = 0; s < $scope.standards.length; s++) {
            $scope.standards[s].pending = 0;
            $scope.standards[s].title = $scope.standards[s].standard.slice(0, $scope.standards[s].standard.length - 2);

            checkpending(s, $scope.standards[s].standardid);


        };
        console.log($scope.standards);
    };
    var getstandardserror = function (data, status) {
        $ionicLoading.hide();
        /*INTERNET ERROR*/
    };
    $ionicLoading.show({
        template: '<ion-spinner class="android"></ion-spinner> <br> Fetching Standards'
    });

    var getstandards = function () {
        MyServices.getstandards().success(getstandardssuccess).error(getstandardserror);
    };

    /*ROUTING*/
    $scope.gotoquestions = function (id, standard) {
        $scope.user.standardid = id;
        $scope.user.standard = standard;
        $.jStorage.set("user", $scope.user);
        $location.path('/app/questions');
    };

    $scope.gotochat = function () {
        $location.path('/app/chatlist');
    };


    /*CHECK NEE MESSAGES*/
    $scope.$on('$ionicView.beforeEnter', function () {


        $scope.newmessages = false;

        filterparameters = {
            'userid': $scope.user.userid,
            'standardid': '',
            'subjectid': '',
            'chapters': '',
            'number': 0,
            'limit': 30,
            'filters': {
                'unver': 0,
                'ans': 0,
                'unans': 0,
                'abt': 0,
                'abst': 0,
                'askt': 0,
                'asks': 0,
                'book': 0,
                'me': 0,
                'trending': 0
            }
        };

        getstandards();

    });

    var devicereadyviewanalyse = function () {
        MyServices.viewanalyse("Teachers Home Page");
    };
    document.addEventListener("deviceready", devicereadyviewanalyse, false);

    var checkfornewmessagessuccess = function (data, status) {
        console.log(data);
        if (data > 0) {
            $scope.newmessages = true;
        } else {
            $scope.newmessages = false;
        };
    };
    var checkfornewmessageserror = function (data, status) {

    };
    var checkfornew = function () {
        MyServices.checkfornewmessages().success(checkfornewmessagessuccess).error(checkfornewmessageserror);
    };
    var refresh = $interval(function () {
        checkfornew();
    }, 15000);

    /*CANCEL ON LEAVE*/
    $scope.$on('$ionicView.beforeLeave', function () {
        $interval.cancel(refresh);

    });
    /*----*/

});
inqapp.controller('feedbackCtrl', function ($scope, $stateParams) {

});




inqapp.controller('profileCtrl', function ($scope, $stateParams, $location, $interval, $ionicPopup, MyServices, $rootScope, $ionicLoading, $ionicSideMenuDelegate, $ionicPopup, $ionicModal, $filter) {

    $ionicSideMenuDelegate.canDragContent(true);

    /*INITIALIZATIONS*/
    $scope.user = $.jStorage.get("user");
    $scope.usertypes = usertypes;

    MyServices.useranalyse($scope.user.userid);

    $scope.profilecounts = {
        'answeredcount': 0,
        'askedcount': 0
    };
    var getprofilecountssuccess = function (data, status) {
        console.log(data);
        $scope.profilecounts = {
            'answeredcount': data.atotal,
            'askedcount': data.qtotal
        };
    };
    var getprofilecountserror = function (data, status) {
        console.log(data);
        MyServices.showToast('Not able to retrive your stats, check Internet Connection');
    };

    $scope.$on('$ionicView.beforeEnter', function () {
        MyServices.getprofilecounts().success(getprofilecountssuccess).error(getprofilecountserror);
    });

    var devicereadyviewanalyse = function () {
        MyServices.viewanalyse("Profile Page");
    };
    document.addEventListener("deviceready", devicereadyviewanalyse, false);

    $scope.changepassworddata = {
        'currpassword': '',
        'newpassword': '',
        'confpassword': ''
    };

    /*GET STANDARDS INITIAL*/
    var getstandardssuccess = function (data, status) {
        console.log(data);
        $scope.standards = data;
    };
    var getstandardserror = function (data, status) {
        $scope.standards = data;
    };
    MyServices.getstandards().success(getstandardssuccess).error(getstandardserror);

    /*ROUTING*/
    $scope.gotoquestionsasked = function () {
        $location.path("/app/questions");
    };

    /*SCENE CHANGES*/
    $scope.editprofile = false;

    var editprofilesuccess = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        if (data == '1') {
            $.jStorage.set("user", $scope.user);
        } else {

            if (data == '2') {
                $scope.user = $scope.tempuser;
                MyServices.showToast("This number is already registered");
            } else {
                $scope.user = $scope.tempuser;
                /*ERROR COULD NOT EDIT USER*/
                MyServices.showToast("Could not edit profile. Try Again");
            };
        };
    };
    var editprofileerror = function (data, status) {
        $ionicLoading.hide();
        $scope.user = $scope.tempuser;
        console.log(data);
        /*INTERNER ERROR*/
        $ionicLoading.hide();
        MyServices.showToast('There might be a problem with your Internet Connection');
    };

    var usertempdata;

    $scope.editmode = function () {

        console.log($scope.user);
        console.log(usertempdata);

        if ($scope.editprofile == true) {
            $ionicLoading.show({
                template: '<ion-spinner class="android"></ion-spinner> <br>Updating Profile'
            });
            if (usertempdata == $scope.user) {
                $ionicLoading.hide();
            } else {
                console.log("EDIT");
                MyServices.editprofile($scope.user).success(editprofilesuccess).error(editprofileerror);
            };

        } else {
            usertempdata = angular.copy($scope.user);
        };
        $scope.editprofile = !$scope.editprofile;

        /*Initialize Select*/
        $interval(function () {
            $('select').material_select();
            $('.select-wrapper .caret').empty();
            $('.select-wrapper .caret').addClass('icon-caret-down');
        }, 100, 1);
    };

    /*SET STANDARD NAME*/
    $scope.setstandard = function () {
        for (var sn = 0; sn < $scope.standards.length; sn++) {
            if ($scope.standards[sn].standardid == $scope.user.standardid) {
                $scope.user.standard = $scope.standards[sn].standard;
            };
        };
        console.log($scope.user.standard);
    };

    /*CHANGE PASSWORD*/
    // An elaborate, custom popup
    $scope.changepassmodal = function () {

        passpopup = $ionicPopup.show({
            templateUrl: 'templates/changepassword.html',
            cssClass: 'passpopup',
            scope: $scope
        });
    };

    var changepasswordsuccess = function (data, status) {
        $ionicLoading.hide();
        if (data == '1') {
            $scope.user.password = $scope.changepassworddata.newpassword;
            $scope.changepassworddata = {
                'currpassword': '',
                'newpassword': '',
                'confpassword': ''
            };
            /*ERROR PASSWORD CHANGED SUCCESSFULLY*/
            MyServices.showToast("Password changed successfully");
            $.jStorage.set("user", $scope.user);
            passpopup.close();
        } else {
            $scope.changepassworddata = {
                'currpassword': '',
                'newpassword': '',
                'confpassword': ''
            };
            /*ERROR COULD NOT CHANGE PASSWORD*/
            MyServices.showToast("Could not change password. Try Again");
        };
    };
    var changepassworderror = function (data, status) {
        $ionicLoading.hide();
        /*ERROR INTERNET*/
        $ionicLoading.hide();
        MyServices.showToast('There might be a problem with your Internet Connection');
    };
    $scope.changepass = function () {
        console.log($scope.changepassworddata);
        if ($scope.changepassworddata.currpassword == $scope.user.password) {
            if ($scope.changepassworddata.newpassword == $scope.changepassworddata.confpassword) {
                if (/^\d+$/.test($scope.changepassworddata.newpassword)) {
                    // A confirm dialog
                    var confirmPopup = $ionicPopup.confirm({
                        title: 'Change Password',
                        template: 'Are you sure you want to change your password ?',
                        cssClass: 'confirmpopup'
                    });

                    confirmPopup.then(function (res) {
                        if (res) {
                            $ionicLoading.show({
                                template: '<ion-spinner class="android"></ion-spinner> <br>Changing Password'
                            });
                            MyServices.changepassword($scope.changepassworddata.newpassword).success(changepasswordsuccess).error(changepassworderror);
                        } else {
                            console.log('You are not sure');
                        }
                    });
                } else {
                    /*PASSWORDS DONT MATCH*/
                    MyServices.showToast("Passwords can be only 4 digit number");
                }


            } else {
                /*PASSWORDS DONT MATCH*/
                MyServices.showToast("Passwords do not match");
            };
        } else {
            /*ERROR CURRENT PASSWORD IS WRONG*/
            MyServices.showToast("Current password entered is incorrect");
        };

    };
    $scope.hidepasswordpopup = function () {
        passpopup.close();
    };

    /*CHANGE NUMBER*/
    // An elaborate, custom popup
    $scope.changenummodal = function () {

        numpopup = $ionicPopup.show({
            templateUrl: 'templates/changenumber.html',
            cssClass: 'passpopup',
            scope: $scope
        });
    };
    $scope.hidenumpopup = function () {
        numpopup.close();
    };

    $scope.gotopaq = function () {
        quesfilterapplied = true;
        filterparameters = {
            'userid': $scope.user.userid,
            'standardid': $scope.user.standardid,
            'subjectid': '',
            'chapters': '',
            'number': 0,
            'limit': 20,
            'filters': {
                'unver': 0,
                'ans': 0,
                'unans': 0,
                'abt': 0,
                'abst': 0,
                'askt': 0,
                'asks': 0,
                'book': 0,
                'me': 1,
                'trending': 0
            }
        };
        console.log(filterparameters);
        $location.path('/app/questions');
    };

    $scope.copypromocode = function () {
        var promocode = $filter('useridtopromocode')($scope.user.userid, $scope.user.name);
        cordova.plugins.clipboard.copy(promocode);
    };

    $scope.mypromocode = $filter('useridtopromocode')($scope.user.userid, $scope.user.name);
    var promomodalview;
    promomodalview = $ionicModal.fromTemplate('<ion-modal-view class="promomodalview"> <ion-header-bar> <h5 class="title lightblackcolor">Your Promo Code Here</h5></ion-header-bar><ion-content><h2 class="textcenter promo-code-color">{{mypromocode}}</h2><div class="promocopybtn" ng-click="copypromocode()"><img src="../img/copy.svg" alt="copy-svg"><span>COPY</span></div><p class="textcenter promocodesharetext">You can share this promo code with your friends & family and they will benefit 15 questions. </p><br><a ng-click="hidepromomodal()"><?xml version="1.0" encoding="UTF-8"?><svg class="promosvg" enable-background="new 0 0 52 52" version="1.1" viewBox="0 0 52 52" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26   S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"/><path d="M35.707,16.293c-0.391-0.391-1.023-0.391-1.414,0L26,24.586l-8.293-8.293c-0.391-0.391-1.023-0.391-1.414,0   s-0.391,1.023,0,1.414L24.586,26l-8.293,8.293c-0.391,0.391-0.391,1.023,0,1.414C16.488,35.902,16.744,36,17,36   s0.512-0.098,0.707-0.293L26,27.414l8.293,8.293C34.488,35.902,34.744,36,35,36s0.512-0.098,0.707-0.293   c0.391-0.391,0.391-1.023,0-1.414L27.414,26l8.293-8.293C36.098,17.316,36.098,16.684,35.707,16.293z"/></svg></a></ion-content></ion-modal-view>', {
        scope: $scope,
        animation: 'slide-in-up'
    });

    $scope.showpromomodal = function () {

        $scope.data = {};
        promomodalview.show();
        /*.then(function (modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function () {
            $scope.modal.show();*/
        //   };
        //  $scope.closeModal = function() {
        //    $scope.modal.hide();
        //  };
        //  // Cleanup the modal when we're done with it!
        //  $scope.$on('$destroy', function() {
        //    $scope.modal.remove();
        //  });
        //  // Execute action on hide modal
        //  $scope.$on('modal.hidden', function() {
        //    // Execute action
        //  });
        //  // Execute action on remove modal
        //  $scope.$on('modal.removed', function() {
        //    // Execute action
        //  });

        $scope.hidepromomodal = function () {


            promomodalview.hide();

        };







    }









});








inqapp.controller('paqCtrl', function ($scope, $stateParams) {});
inqapp.controller('bookmarksCtrl', function ($scope, $stateParams) {});
inqapp.controller('trendingCtrl', function ($scope, $stateParams) {

    var gettrendingquestionsuccess = function (data, status) {
        console.log(data);
        $scope.questions = data;
    };
    var gettrendingquestionerror = function (data, status) {
        console.log(data);
    };
    MyServices.gettrendingquestion().success(gettrendingquestionsuccess).error(gettrendingquestionerror);


});
inqapp.controller('pendingCtrl', function ($scope, $stateParams, MyServices, $location) {



    var getpendingsuccess = function (data, status) {
        console.log(data);
        $scope.questions = data;
    };
    var getpendingerror = function (data, status) {
        console.log(data);
    };

    var getpending = function () {
        MyServices.getpending($scope.user.standardid).success(getpendingsuccess).error(getpendingerror);
    };

    $scope.$on('$ionicView.beforeEnter', function () {

        $scope.user = $.jStorage.get("user");
        $scope.usertypes = usertypes;
        console.log($scope.user);
        getpending();
    });


    $scope.gotoanswers = function (id, ind) {
        $('.youtubevideo').each(function () {
            $(this).attr('src', $('iframe').attr('src'));
        });
        if ($scope.questions[ind].premium == 0 || $scope.user.userstype != '3') {
            offsetvalue = -1 * $('.questionslist').offset().top;
            $location.path("/app/questions/:" + id);
        } else {
            var premiumPopup = $ionicPopup.alert({
                title: 'Exclusive Content',
                template: 'You need to register at Sunalis Classes to access Exclusive Content',
                cssClass: 'confirmpopup'
            });
        };
    };

    /*Share in Pending*/
    $scope.sharequestion = function (qid) {
        $cordovaSocialSharing.share('Check out this question on Sunalis Classes App', 'Sunalis Classes Question', null, 'http://www.sunalisclasses.com/sharedoubt.php?question=' + qid);
    };

    /*LIKE A QUESTION FROM PENDING PAGE*/
    $scope.starquestion = function (qid, index) {

        var starquestionsuccess = function (data, status) {
            console.log(data);
            if (data != '1') {
                $scope.questions[index].starcount += $scope.questions[index].starred == '1' ? -1 : 1;
                $scope.questions[index].starred = $scope.questions[index].starred == '1' ? 0 : 1;
                MyServices.showToast("Could not thank question.");
            };
        };

        var starquestionerror = function (data, status) {
            console.log(data);
            $scope.questions[index].starcount += $scope.questions[index].starred == '1' ? -1 : 1;
            $scope.questions[index].starred = $scope.questions[index].starred == '1' ? 0 : 1;
            MyServices.showToast("Could not thank question.");
        };



        $scope.questions[index].starcount += $scope.questions[index].starred == '1' ? -1 : 1;
        $scope.questions[index].starred = $scope.questions[index].starred == '1' ? 0 : 1;

        if ($scope.questions[index].starred == '1') {
            $('.like-icon-' + index).addClass('like-animation');
            setTimeout(function () {
                $('.like-icon-' + index).removeClass('like-animation');
            }, 150);
        };

        console.log($scope.questions[index].starcount);

        MyServices.starquestion($scope.user.userid, qid).success(starquestionsuccess).error(starquestionerror);
    };

    /*BOOKMARK QUESTION FROM PENDING PAGE*/
    var bind;
    var bq;
    var bookmarkquessuccess = function (data, status) {
        console.log(data);
        if (data == 0) {
            MyServices.showToast("Could not bookmark question");
        } else {
            if ($scope.questions[bind].bookmarked == 0) {
                $scope.questions[bind].bookmarked = 1;
            } else {
                $scope.questions[bind].bookmarked = 0;
            };
        }
    };
    var bookmarkqueserror = function (data, status) {
        console.log(data);
    };
    $scope.bookmarkques = function (qid) {
        console.log(qid);
        for (var bk = 0; bk < $scope.questions.length; bk++) {
            if ($scope.questions[bk].questionid == qid) {
                bind = bk;
                break;
            };
        };
        bq = qid;
        MyServices.bookmarkques($scope.user.userid, qid).success(bookmarkquessuccess).error(bookmarkqueserror);
    };

});
inqapp.controller('askCtrl', function ($scope, $stateParams, MyServices, $location, $ionicLoading, $interval, $rootScope, $ionicHistory, $cordovaVibration, $ionicPopup, $filter) {

    /*INITIALIZATIONS*/
    $scope.user = $.jStorage.get("user");

    $scope.subjectname = "";
    $scope.chaptername = "";

    /*SUBJECTS GET*/
    var getusersubjectssuccess = function (data, status) {
        console.log(data);
        $scope.subjects = data;
        for (var sd = 0; sd < $scope.subjects.length; sd++) {
            $scope.subjects[sd].isactive = true;
            for (var cd = 0; cd < $scope.subjects[sd].chapters.length; cd++) {
                $scope.subjects[sd].chapters[cd].selected = false;
            };
        };
        console.log($scope.subjects);
    };
    var getusersubjectserror = function (data, status) {
        console.log(data);
    };

    if ($.jStorage.get("bottombar") == null || $.jStorage.get("bottombar") == []) {
        //$scope.subjects = tempusersubjects;
        MyServices.getusersubjects($scope.user.standardid).success(getusersubjectssuccess).error(getusersubjectserror);
    } else {
        //$scope.subjects = tempusersubjects;
        $scope.subjects = $.jStorage.get("bottombar");
    };

    $scope.$on('$ionicView.beforeEnter', function () {

        $scope.askques = {
            'question': '',
            'image': null,
            'chapterid': 0,
            'imageview': 0,
            'premium': 0,
            'trending': 0,
            'video': null
        };

        MyServices.useranalyse($scope.user.userid);

        $scope.showimg = false;

        $scope.showimg = false;

        var pictureSource; // picture source
        var destinationType; // sets the format of returned value

        document.addEventListener("deviceready", onDeviceReady, false);

        $scope.showchapters = false;
        $scope.filterapplied = false;
        $scope.selectedsubject = null;
        $scope.dummychapterprefix = "";
        $scope.allchapsbutton = true;

        if ($.jStorage.get("bottombar") == null || $.jStorage.get("bottombar") == []) {
            //$scope.subjects = tempusersubjects;
            MyServices.getusersubjects($scope.user.standardid).success(getusersubjectssuccess).error(getusersubjectserror);
        } else {
            //$scope.subjects = tempusersubjects;
            $scope.subjects = $.jStorage.get("bottombar");
        };
    });

    $scope.selectallchaps = function () {
        for (var c = 0; c < $scope.chapters.length; c++) {
            $scope.chapters[c].selected = true;
        };
        $scope.allchapsbutton = false;
    };
    $scope.addtochapfilter = function () {
        $scope.allchapsbutton = false;
        for (var c = 0; c < $scope.chapters.length; c++) {
            if ($scope.chapters[c].selected == false) {
                $scope.allchapsbutton = true;
            } else {
                $scope.allchapsbutton = false;
            };
        };
    };

    $scope.removefilters = function () {
        $scope.showchapters = false;
    };

    $scope.selectsubject = function (index) {
        $scope.subjectname = $scope.subjects[index].subject;


        if ($scope.showchapters) {
            if ($scope.selectedsubject == index) {
                $scope.showchapters = false;
            } else {
                $scope.chaptername = '';
                $scope.askques.chapterid = 0;
                $scope.chapters = $scope.subjects[index].chapters;

                for (var sc = 0; sc < $scope.chapters.length; sc++) {
                    if ($scope.chapters[sc].chaptertitle == "Uncategorized") {
                        var schapid = $scope.chapters[sc].chapterid;
                        var schapname = $scope.chapters[sc].chaptertitle;
                    };
                };
                if (index != $scope.selectedsubject) {
                    $scope.chaptername = schapname;
                    $scope.askques.chapterid = schapid;
                };

                $scope.selectedsubject = index;
            };
        } else {
            $scope.showchapters = true;

            $scope.chapters = $scope.subjects[index].chapters;

            for (var sc = 0; sc < $scope.chapters.length; sc++) {
                if ($scope.chapters[sc].chaptertitle == "Uncategorized") {
                    var schapid = $scope.chapters[sc].chapterid;
                    var schapname = $scope.chapters[sc].chaptertitle;
                };
            };
            console.log(schapname);
            console.log(schapid);
            if (index != $scope.selectedsubject) {
                $scope.chaptername = schapname;
                $scope.askques.chapterid = schapid;
            };

            $scope.selectedsubject = index;

        };



        $scope.subjects[index].isactive = true;

        /*get chapters*/
        var getchapterssuccess = function (data, status) {
            $scope.chapters = data;
        };
        var getchapterserror = function (data, status) {
            $scope.chapters = data;
        };
        //$scope.getchapters($scope.subjects[index].id).success(getchapterssuccess).error(getchapterserror);
    };

    /*Open FACULTY ADVANCED OPTIONS POPUP */
    $scope.openadvancedoptions = function () {

        $scope.askques.imageview = $filter('truefalse')($scope.askques.imageview);
        $scope.askques.premium = $filter('truefalse')($scope.askques.premium);
        $scope.askques.trending = $filter('truefalse')($scope.askques.trending);
        $scope.askques.videotype = $filter('truefalse')($scope.askques.videotype);

        var advancedoptions = $ionicPopup.show({
            template: '<ion-toggle ng-model="askques.imageview" toggle-class="toggle-primary">Full Image View</ion-toggle> <ion-toggle ng-model="askques.premium" toggle-class="toggle-calm">Exclusive Question</ion-toggle> <ion-toggle ng-model="askques.trending" toggle-class="toggle-calm">Trending Question</ion-toggle> <ion-toggle ng-model="askques.videotype" toggle-class="toggle-calm">Video Question</ion-toggle>',
            title: 'Advanced Filters',
            scope: $scope,
            cssClass: 'advanced-filter-popup',
            buttons: [
                {
                    text: '<b>Save</b>',
                    type: 'button-positive',
                    onTap: function (e) {

                        /*Check Full VIiw Image*/
                        if ($scope.askques.imageview) {
                            $scope.askques.imageview = 1;
                        } else {
                            $scope.askques.imageview = 0;
                        };
                        /*Check Premium Question*/
                        if ($scope.askques.premium) {
                            $scope.askques.premium = 1;
                        } else {
                            $scope.askques.premium = 0;
                        };
                        /*Check Trending Question*/
                        if ($scope.askques.trending) {
                            $scope.askques.trending = 1;
                        } else {
                            $scope.askques.trending = 0;
                        };
                        /*Check Video Question*/
                        if ($scope.askques.videotype) {
                            $scope.askques.videotype = 1;
                            $scope.askques.image = null;
                        } else {
                            $scope.askques.videotype = 0;
                            $scope.askques.video = null;
                        };
                    }
              }
            ]
        });
    };

    /*FUNCTIONS*/

    /*SET CHAP ID*/
    $scope.setchapid = function (id) {
        console.log('chapter id is ' + id);
        $scope.askques.chapterid = id;
    };

    /*SHOW CHAPTER*/
    $scope.selectchap = function (index) {
        $scope.chaptername = $scope.chapters[index].chaptertitle;
    };

    /*CALLBACK FUNCTIONS*/
    decreasepaidquestionssuccess = function (response, status) {
        $rootScope.paidquestions = parseInt(response.data);
    };
    decreasepaidquestionserror = function (error, status) {

    };




    /*SEND QUESTION*/
    var askquestionsuccess = function (data, status) {
        console.log(data);
        $ionicLoading.hide();
        if (data == "1") {

            if ($scope.user.userid == 3 && $rootScope.questionsleft <= 0) { //Subtract from extraquestions
                MyServices.decreasepaidquestions($scope.userdata.userid).success(decreasepaidquestionssuccess).error(decreasepaidquestionserror);
            }
            // Vibrate 100ms
            //$cordovaVibration.vibrate(100);

            $rootScope.$emit("checkleft", {});

            $location.path("/app/questions");
        } else {
            /*ERROR*/
            MyServices.showToast("Question not posted");
        };
    };
    var askquestionerror = function (data, status) {
        console.log(data);
        $ionicLoading.hide();
        MyServices.showToast('There might be a problem with your Internet Connection');
        /*INTERNET ERROR*/
    };


    /*UPLOAD IMAGE*/
    $scope.imageuploaded = false;
    $scope.fileURI = '';

    $scope.filetransferdone = function (data) {

        console.log("SUCCESS");
        console.log(data);
        $scope.askques.image = data.response;

        console.log($scope.askques);

        MyServices.askquestion($scope.user.userid, $scope.askques.question, $scope.askques.chapterid, $scope.askques.image, $scope.askques.imageview, $scope.askques.premium, $scope.askques.trending, $scope.askques.video).success(askquestionsuccess).error(askquestionerror);
    };



    /*FILE TRANSFER*/

    $scope.filetransfer = function () {

        var win = function (data, status) {
            $scope.fileURI = '';
            $ionicLoading.hide();
            $scope.filetransferdone(data);
        };
        var fail = function (error) {
            $ionicLoading.hide();
            console.log(error);
            MyServices.showToast('There seems to be a problem with the internet');
        };

        console.log($scope.fileURI);
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Uploading Image...'
        });

        $scope.imgname = $scope.fileURI.substr($scope.fileURI.lastIndexOf('/') + 1);

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = $scope.imgname;
        options.mimeType = "image/jpeg";

        var params = {};
        params.value1 = "test";
        params.value2 = "param";

        options.params = params;
        options.chunkedMode = false;

        console.log(options);

        if ($scope.fileURI.lastIndexOf('?') > 0) {
            $scope.fileURI.substr(0, $scope.fileURI.lastIndexOf('?'));
        };

        var ft = new FileTransfer();
        ft.upload($scope.fileURI, encodeURI("http://learnwithinq.com/appfilesv2/uploadimage.php"), win, fail, options, true);
    };


    $scope.sendquestion = function () {

        if ($scope.imageuploaded == true) {

            if ($scope.askques.chapterid != 0) {

                /*CALL TO OTHER FUNCTION*/
                $scope.filetransfer();

            } else {
                /*ERROR FILL QUESTION AND CHAPTER*/
                MyServices.showToast("You need to select Chapter");
            };

        } else {
            if ($scope.askques.chapterid != 0) {
                console.log($scope.askques);
                $ionicLoading.show({
                    template: '<ion-spinner class="android"></ion-spinner> <br>Adding Question'
                });
                MyServices.askquestion($scope.user.userid, $scope.askques.question, $scope.askques.chapterid, $scope.askques.image, $scope.askques.imageview, $scope.askques.premium, $scope.askques.trending, $scope.askques.video).success(askquestionsuccess).error(askquestionerror);
            } else {
                /*ERROR FILL QUESTION AND CHAPTER*/
                MyServices.showToast("You need to select Chapter");
            };
        };
    };

    function onDeviceReady() {
        pictureSource = navigator.camera.PictureSourceType;
        destinationType = navigator.camera.DestinationType;

        MyServices.viewanalyse("Ask Question Page");
    }

    function clearCache() {
        navigator.camera.cleanup();
    }

    var retries = 0;

    var onCapturePhoto = function (fileURI) {
        console.log(fileURI);
        $scope.fileURI = fileURI;

        /*CALL TO OTHER FUNCTION*/
        $scope.imageuploaded = true;

        $scope.showimg = true;
        var image = document.getElementById('askImage');
        console.log(image);
        if (image != null) {
            image.src = $scope.fileURI;

            $scope.$apply();
        };

        clearCache();

    };
    var onFail = function (message) {
        alert('Failed because: ' + message);
    };

    $scope.capturephoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            allowEdit: true,
            correctOrientation: true
        });
    };
    $scope.pickphoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            correctOrientation: true
        });
    };

    /*CLEAR CACHE*/
    $scope.$on('$ionicView.beforeLeave', function () {
        $ionicHistory.clearCache();
        $scope.imageuploaded = false;
        $scope.showimg = false;
    });



});
inqapp.controller('filterCtrl', function ($scope, $stateParams, MyServices, $location, $ionicLoading) {

    /*SUBJECTS GET*/
    var getusersubjectssuccess = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        $scope.subjects = data;
        for (var sd = 0; sd < $scope.subjects.length; sd++) {
            $scope.subjects[sd].isactive = true;
            for (var cd = 0; cd < $scope.subjects[sd].chapters.length; cd++) {
                $scope.subjects[sd].chapters[cd].selected = false;
            };
        };
        console.log($scope.subjects);
    };
    var getusersubjectserror = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
    };

    var devicereadyviewanalyse = function () {
        MyServices.viewanalyse("Filter Page");
    };
    document.addEventListener("deviceready", devicereadyviewanalyse, false);

    /*ON ENTER FILTERS*/
    $scope.$on('$ionicView.beforeEnter', function () {
        // Code you want executed every time view is opened
        $scope.user = $.jStorage.get("user");
        $scope.usertypes = usertypes;

        $scope.filters = {
            'unver': false,
            'ans': false,
            'unans': false,
            'abt': false,
            'abst': false,
            'askt': false,
            'asks': false,
            'book': false,
            'me': false,
            'trending': 0
        };

        $scope.showchapters = false;
        $scope.filterapplied = false;
        $scope.selectedsubject = null;
        $scope.selectedsubjectid = null;
        $scope.dummychapterprefix = "";
        $scope.allchapsbutton = true;

        $scope.chapters = [];

        var fillist = ['unverified', 'answered', 'unanswered', 'answeredbyteacher', 'answeredbystudent', 'askedbyteacher', 'askedbystudent', 'bookmarked', 'me'];

        for (var cf = 0; cf < fillist.length; cf++) {
            $('#' + fillist[cf]).prop('checked', false);
        };



        if ($.jStorage.get("bottombar") == null || $.jStorage.get("bottombar") == []) {
            $ionicLoading.show({
                template: '<ion-spinner class="android"></ion-spinner> <br>Fetching Subjects'
            });
            //$scope.subjects = tempusersubjects;
            MyServices.getusersubjects($scope.user.standardid).success(getusersubjectssuccess).error(getusersubjectserror);
        } else {
            //$scope.subjects = tempusersubjects;
            $scope.subjects = $.jStorage.get("bottombar");
        };
    });

    /*SUBJECT QUESTIONS FILTER*/

    $scope.showchapters = false;
    $scope.filterapplied = false;
    $scope.selectedsubject = null;
    $scope.selectedsubjectid = null;
    $scope.dummychapterprefix = "";
    $scope.allchapsbutton = true;

    $scope.chapters = [];

    $scope.selectallchaps = function () {
        for (var c = 0; c < $scope.chapters.length; c++) {
            $scope.chapters[c].selected = true;
        };
        $scope.allchapsbutton = false;
    };

    $scope.addtochapfilter = function () {
        $scope.allchapsbutton = false;
        console.log("change");
        for (var c = 0; c < $scope.chapters.length; c++) {
            if ($scope.chapters[c].selected == false) {
                $scope.allchapsbutton = true;
                break;
            } else {
                $scope.allchapsbutton = false;
            };
        };
    };

    $scope.addtofilter = function (obj) {
        console.log(obj);
        if ($scope.filters[obj] == false) {
            $scope.filters[obj] = true;
        } else {
            $scope.filters[obj] = false;
        };
        console.log($scope.filters);
    };

    $scope.clearfilters = function () {

        $scope.showchapters = false;
        $scope.filterapplied = false;
        $scope.selectedsubject = null;
        $scope.selectedsubjectid = null;
        $scope.dummychapterprefix = "";
        $scope.allchapsbutton = true;

        $scope.chapters = [];

        var fillist = ['unverified', 'answered', 'unanswered', 'answeredbyteacher', 'answeredbystudent', 'askedbyteacher', 'askedbystudent', 'bookmarked', 'me'];

        for (var cf = 0; cf < fillist.length; cf++) {
            $('#' + fillist[cf]).prop('checked', false);
        };

        $scope.filters = {
            'unver': false,
            'ans': false,
            'unans': false,
            'abt': false,
            'abst': false,
            'askt': false,
            'asks': false,
            'book': false,
            'me': false,
            'trending': 0
        };

        filterparameters = {
            'userid': $scope.user.userid,
            'standardid': $scope.user.standardid,
            'subjectid': '',
            'chapters': '',
            'number': 0,
            'limit': 20,
            'filters': ''
        };

        $location.path('/app/questions');
    };

    $scope.addfilters = function () {
        var filterslist = ['unver', 'ans', 'unans', 'abt', 'abst', 'askt', 'asks', 'book', 'me'];
        if ($scope.selectedsubject != null) {

            $scope.chapfilter = [];

            for (var cc = 0; cc < $scope.chapters.length; cc++) {
                if ($scope.chapters[cc].selected) {
                    $scope.chapfilter.push($scope.chapters[cc].chapterid);
                };
            };

            /*for (var fl = 0; fl < filterslist.length; fl++) {
                if ($scope.filters[filterslist[fl]] == true) {
                    $scope.filters[filterslist[fl]] = 1;
                } else {
                    $scope.filters[filterslist[fl]] = 0;
                };
            };*/

            var fillist = ['unverified', 'answered', 'unanswered', 'answeredbyteacher', 'answeredbystudent', 'askedbyteacher', 'askedbystudent', 'bookmarked', 'me'];
            var filnamelist = ['unver', 'ans', 'unans', 'abt', 'abst', 'askt', 'asks', 'book', 'me'];

            for (var cf = 0; cf < fillist.length; cf++) {
                if ($('#' + fillist[cf]).prop('checked')) {
                    $scope.filters[filnamelist[cf]] = 1;
                } else {
                    $scope.filters[filnamelist[cf]] = 0;
                };
            };

            console.log($scope.chapfilter);
            console.log($scope.filters);


            /*PLAYLISTS PAGE FILTER*/
            console.log($scope.selectedsubjectid);

            quesfilterapplied = true;
            filterparameters = {
                'userid': $scope.user.userid,
                'standardid': $scope.user.standardid,
                'subjectid': $scope.selectedsubjectid,
                'chapters': $scope.chapfilter,
                'number': 0,
                'limit': 20,
                'filters': $scope.filters
            };

            console.log(filterparameters);

            $location.path("/app/questions");
        } else {
            /*ERROR SELECT SUBJECT*/
            MyServices.showToast("Select subject and chapters");
        };
    };

    $scope.selectsubject = function (index, subid) {

        $scope.allchapsbutton = true;

        if ($scope.showchapters) {
            if ($scope.selectedsubject == index) {
                $scope.showchapters = false;
                $scope.selectedsubject = null;
                $scope.selectedsubjectid = null;
                for (var c = 0; c < $scope.chapters.length; c++) {
                    $scope.chapters[c].selected = false;
                };
                $scope.chapters = [];
            } else {
                $scope.chapters = $scope.subjects[index].chapters;
                for (var c = 0; c < $scope.chapters.length; c++) {
                    $scope.chapters[c].selected = false;
                };
                $scope.selectedsubject = index;
                $scope.selectedsubjectid = subid;
            };
        } else {
            $scope.showchapters = true;

            $scope.chapters = $scope.subjects[index].chapters;
            console.log($scope.chapters);
            $scope.selectedsubject = index;
            $scope.selectedsubjectid = subid;
        };

        $scope.subjects[index].isactive = true;
    };


});
inqapp.controller('chatlistCtrl', function ($scope, $stateParams, MyServices, $location, $interval, $ionicLoading) {

    $scope.user = $.jStorage.get("user");

    $scope.usertypes = usertypes;

    var getchatlistsuccess = function (data, status) {
        $scope.chatlist = [];
        $scope.chatlist = data;
        console.log($scope.chatlist);
    };
    var getchatlisterror = function (data, status) {
        /*INTERNET ERROR*/
        MyServices.showToast('There might be a problem with your Internet Connection');
    };
    $scope.getcl = function () {
        MyServices.getchatlist($scope.user.userid).success(getchatlistsuccess).error(getchatlisterror);
    };

    $scope.$on('$ionicView.beforeEnter', function () {
        $scope.getcl();
    });
    $scope.getcl();

    var devicereadyviewanalyse = function () {
        MyServices.viewanalyse("Chat List Page");
    };
    document.addEventListener("deviceready", devicereadyviewanalyse, false);


    /*ROUTING*/
    $scope.gotochat = function (id, name) {
        recieverid = id;
        recievername = name;
        $location.path('/app/chat');
    };

    var refresh = $interval(function () {
        $scope.getcl()
    }, 3000);

    $scope.$on('$ionicView.beforeLeave', function () {
        $interval.cancel(refresh);
    });
});

inqapp.controller('chatCtrl', function ($scope, $stateParams, MyServices, $interval, $rootScope, $ionicModal, $location, $ionicHistory, $ionicLoading, $filter, $ionicPopup) {



    $scope.$on('$ionicView.beforeEnter', function () {


        //document.addEventListener("deviceready", onDeviceReady, false);
        $scope.showimg = false;

        var pictureSource; // picture source
        var destinationType; // sets the format of returned value

        document.addEventListener("deviceready", onDeviceReady, false);



    });


    $scope.usertypes = usertypes;
    $scope.messages = [];

    // Code you want executed every time view is opened
    $scope.user = $.jStorage.get("user");
    $scope.showimg = false;

    if ($scope.user.userstype == 1) {
        $scope.senderimage = usertypes[0].image;
        $scope.recieverimage = usertypes[1].image;
    } else {
        $scope.senderimage = usertypes[1].image;
        $scope.recieverimage = usertypes[0].image;
    }

    console.log($scope.user.userstype);
    if ($scope.user.userstype == 1) {
        $scope.tid = $scope.user.userid;
        $scope.sid = recieverid;
    } else {
        $scope.tid = recieverid;
        $scope.sid = $scope.user.userid;
    };
    $scope.recievername = recievername;

    $scope.messagedata = {
        'message': '',
        'image': null,
        'teacherid': $scope.tid,
        'studentid': $scope.sid,
        'senderid': $scope.user.userid
    };

    $ionicLoading.show({
        template: '<ion-spinner class="android"></ion-spinner> <br>Retrieving Chat'
    });
    var ilh = 0;

    var pictureSource; // picture source
    var destinationType; // sets the format of returned value


    /*OPEN IMAGE POPUP*/

    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.openModal = function () {
        $scope.modal.show();
    };

    $scope.closeModal = function () {
        $scope.modal.hide();
    };


    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function () {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
        // Execute action
    });
    $scope.$on('modal.shown', function () {
        console.log('Modal is shown!');
    });
    $scope.showImage = function (img) {
        console.log(img);
        $scope.imageSrc = img;
        $scope.openModal();
    };


    /*GET MESSAGES*/
    var getmessagessuccess = function (data, status) {

        console.log(data);

        if (data == null || data.length == 0) {
            $scope.messages[0] = {
                createdDate: "03:53 pm",
                image: null,
                is_studentread: 0,
                is_teacherread: 0,
                message: "Hello " + $scope.user.name + ", we haven't spoken yet. You can ask me any doubt you have.",
                messageid: 0,
                senderid: 0,
                studentid: 0,
                teacherid: 0
            };
            $ionicLoading.hide();
        } else {
            if ($scope.messages.length != data.length) {

                if ($scope.messages.length == 0) {
                    scrollspeed = 100;
                } else {
                    scrollspeed = 1000;
                };
                $scope.messages = data;

                $interval(function () {
                    $('.chatdiv').animate({
                        scrollTop: $('.chatdiv')[0].scrollHeight
                    }, scrollspeed);
                    if (ilh == 0) {
                        $ionicLoading.hide();
                        ilh = 1;
                    };
                }, 1000, 1);
            }
        };
    };
    var getmessageserror = function (data, status) {
        if (ilh == 0) {
            $ionicLoading.hide();
            ilh = 1
        };
        console.log(data);
    };
    $scope.getmsgs = function () {
        console.log($scope.tid);
        console.log($scope.sid);
        MyServices.getmessages($scope.tid, $scope.sid).success(getmessagessuccess).error(getmessageserror);
    };
    $scope.getmsgs();

    var refresh = $interval(function () {
        $scope.getmsgs()
    }, 10000);


    /*SEND MESSAGE*/

    $scope.imageuploaded = false;
    $scope.fileURI = '';

    var sendmessagesuccess = function (data, status) {
        $scope.showimg = false;
        console.log(data);
        if (data == 1) {
            $scope.messagedata = {
                'message': '',
                'image': null,
                'teacherid': $scope.tid,
                'studentid': $scope.sid,
                'senderid': $scope.user.userid
            };
            $('#answer').css('height', '30px');
            $scope.getmsgs();
        };

    };
    var sendmessageerror = function (data, status) {
        console.log(data);
    };
    $scope.sendmessage = function () {
        if ($scope.messagedata.message != '') {
            console.log($scope.messagedata);
            MyServices.sendmessage($scope.messagedata).success(sendmessagesuccess).error(sendmessageerror);
        } else {
            /*ERROR TYPE A MESSAGE*/
            MyServices.showToast('Type a message before send');
        };

    };


    $scope.filetransferdone = function (data) {

        console.log("SUCCESS");
        console.log(data);
        $scope.messagedata.image = data.response;

        console.log($scope.messagedata);
        MyServices.sendmessage($scope.messagedata).success(sendmessagesuccess).error(sendmessageerror);
    };

    $scope.gotochatlist = function () {
        $location.path('/app/chatlist');
    };

    /*FILE TRANSFER*/
    $scope.filetransfer = function () {

        var win = function (data, status) {
            $scope.fileURI = '';
            $ionicLoading.hide();
            $scope.filetransferdone(data);

        };
        var fail = function (error) {
            $ionicLoading.hide();
            console.log(error);
            MyServices.showToast('There seems to be a problem with the internet');
        };

        console.log($scope.fileURI);
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Uploading Image...'
        });

        $scope.imgname = $scope.fileURI.substr($scope.fileURI.lastIndexOf('/') + 1);

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = $scope.imgname;
        options.mimeType = "image/jpeg";

        var params = {};
        params.value1 = "test";
        params.value2 = "param";

        options.params = params;
        options.chunkedMode = false;

        console.log(options);

        /*if ($scope.fileURI.lastIndexOf('?') > 0) {
            $scope.fileURI.substr(0, $scope.fileURI.lastIndexOf('?'));
        };*/

        var ft = new FileTransfer();
        ft.upload($scope.fileURI, encodeURI("http://learnwithinq.com/appfilesv2/uploadimage.php"), win, fail, options, true);
    };

    function onDeviceReady() {
        pictureSource = navigator.camera.PictureSourceType;
        destinationType = navigator.camera.DestinationType;

        MyServices.viewanalyse("Chat Page");
    }

    function clearCache() {
        navigator.camera.cleanup();
    }

    var retries = 0;

    var onCapturePhoto = function (fileURI) {
        console.log(fileURI);
        $scope.fileURI = fileURI;

        /*CALL TO OTHER FUNCTION*/
        $scope.imageuploaded = true;

        $scope.showimg = true;
        var image = document.getElementById('myImage');
        console.log(image);
        if (image != null) {
            image.src = $scope.fileURI;

            $scope.$apply();
        };

        clearCache();
        $scope.filetransfer();

    };
    var onFail = function (message) {
        alert('Failed because: ' + message);
    };


    $scope.capturephoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            allowEdit: true,
            correctOrientation: true
        });
    };
    $scope.pickphoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            correctOrientation: true
        });
    };


    /*DOWNLOAD IMAGE*/
    $scope.downloadimage = function () {
        console.log("downloading image");
        var fileTransfer = new FileTransfer();
        var image = $filter('imagepath')($scope.imageSrc);
        var uri = encodeURI(image);
        var fileURL = cordova.file.externalRootDirectory + 'Download/' + $scope.imageSrc;

        fileTransfer.download(
            uri,
            fileURL,
            function (entry) {

                window.MediaScannerPlugin.scanFile(
                    function (msg) {
                        console.log(msg);
                    },
                    function (err) {
                        console.log(err);
                    }
                );

                var completePopup = $ionicPopup.alert({
                    title: 'Image Downloaded',
                    template: 'This image has been downloaded to your device memory',
                    cssClass: 'confirmpopup'
                });
                console.log("download complete: " + entry.toURL());
            },
            function (error) {
                console.log("download error source " + error.source);
                console.log("download error target " + error.target);
                console.log("download error code" + error.code);
                var completePopup = $ionicPopup.alert({
                    title: 'Image Not Downloaded',
                    template: 'This image could not be downloaded.Try Again.',
                    cssClass: 'confirmpopup'
                });
            },
            false, {
                headers: {
                    "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                }
            }
        );
    };

    $scope.$on('$ionicView.beforeLeave', function () {
        $ionicHistory.clearCache();
        $interval.cancel(refresh);
        $scope.showimg = false;
        $scope.imageuploaded = false;
        $ionicHistory.clearCache();
    });




});

inqapp.controller('editCtrl', function ($scope, $stateParams, MyServices, $location, $filter, $ionicHistory, $ionicLoading, $ionicPopup) {


    $scope.user = $.jStorage.get("user");

    MyServices.useranalyse($scope.user.userid);


    $scope.usertypes = usertypes;

    $scope.showimg = false;

    $scope.editdata = {
        'chapterid': 0,
        'answer': '',
        'question': '',
        'image': 'null',
        'imageview': 0,
        'premium': 0,
        'trending': 0,
        'video': null
    };

    var call = 0;

    $scope.showimg = false;

    var pictureSource; // picture source
    var destinationType; // sets the format of returned value

    document.addEventListener("deviceready", onDeviceReady, false);

    /*SUBJECTS GET*/
    var getusersubjectssuccess = function (data, status) {
        console.log(data);
        $scope.subjects = data;
        for (var sd = 0; sd < $scope.subjects.length; sd++) {
            $scope.subjects[sd].isactive = true;
            for (var cd = 0; cd < $scope.subjects[sd].chapters.length; cd++) {
                $scope.subjects[sd].chapters[cd].selected = false;
            };
        };
        console.log($scope.subjects);
        call++;

        if (call == 2) {
            opentray();
        };
    };
    var getusersubjectserror = function (data, status) {
        console.log(data);
    };
    MyServices.getusersubjects($scope.user.standardid).success(getusersubjectssuccess).error(getusersubjectserror);

    /*GET CURRENT DATA*/
    var getquestiondatasuccess = function (data, status) {
        $ionicLoading.hide();
        console.log(data);

        $scope.editdata = data;

        $scope.editdata.imageview = $filter('truefalse')($scope.editdata.imageview);
        $scope.editdata.premium = $filter('truefalse')($scope.editdata.premium);
        $scope.editdata.trending = $filter('truefalse')($scope.editdata.trending);
        if ($scope.editdata.video == null) {
            $scope.editdata.videotype = false;
        } else {
            $scope.editdata.videotype = true;
        };

        call++;
        if ($scope.editdata.image != null) {
            var imageq = document.getElementById('editImage');
            imageq.src = $filter('imagepath')($scope.editdata.image);
        };

        $scope.subjectname = $scope.editdata.subject;
        $scope.chaptername = $scope.editdata.qchapname;

        if (data.image != null) {
            $scope.showimg = true;

        };

        if (call == 2) {
            //opentray();
        };

    };

    var opentray = function () {
        /*OPEN CHAPTER TRAY*/
        $scope.showchapters = true;
        for (var si = 0; si < $scope.subjects.length; si++) {
            if ($scope.subjects[si].subject == $scope.editdata.subject) {
                $scope.subjects[si].isactive = true;
                $scope.selectedsubject = si;
                $scope.chapters = $scope.subjects[si].chapters;
            } else {
                $scope.subjects[si].isactive = false;
            };
            /*for (var cd = 0; cd < $scope.subjects[sd].chapters.length; cd++) {
                $scope.subjects[sd].chapters[cd].selected = false;
            };*/
        };
        /*$scope.chapters = $scope.subjects[index].chapters;
            $scope.selectedsubject = index;*/
    };
    var getquestiondataerror = function (data, status) {
        $ionicLoading.hide();
        MyServices.showToast('There might be a problem with your Internet Connection');
        console.log(data);
        /*INTERNET ERROR*/
    };
    var getanswerdatasuccess = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        $scope.editdata = data;

        if ($scope.editdata.image != null) {
            var imagea = document.getElementById('editImage');
            imagea.src = $filter('imagepath')($scope.editdata.image);
        };
    };
    var getanswerdataerror = function (data, status) {
        $ionicLoading.hide();
        MyServices.showToast('There might be a problem with your Internet Connection');
        console.log(data);
        /*INTERNET ERROR*/
    };

    $scope.edittype = edittype;
    $ionicLoading.show({
        template: '<ion-spinner class="android"></ion-spinner> <br>Fetching data'
    });
    if (edittype == "question") {
        /*GET QUESTION DATA*/
        MyServices.getquestiondata(questionid).success(getquestiondatasuccess).error(getquestiondataerror);
    } else {
        /*GET ANSWER DATA*/
        MyServices.getanswerdata(editid).success(getanswerdatasuccess).error(getanswerdataerror);
    };

    /*Open FACULTY ADVANCED OPTIONS POPUP */
    $scope.openadvancedoptions = function () {

        $scope.editdata.imageview = $filter('truefalse')($scope.editdata.imageview);
        $scope.editdata.premium = $filter('truefalse')($scope.editdata.premium);
        $scope.editdata.trending = $filter('truefalse')($scope.editdata.trending);
        $scope.editdata.videotype = $filter('truefalse')($scope.editdata.videotype);

        var advancedoptions = $ionicPopup.show({
            template: '<ion-toggle ng-model="editdata.imageview" toggle-class="toggle-primary">Full Image View</ion-toggle> <ion-toggle ng-model="editdata.premium" toggle-class="toggle-calm">Exclusive Question</ion-toggle> <ion-toggle ng-model="editdata.trending" toggle-class="toggle-calm">Trending Question</ion-toggle> <ion-toggle ng-model="editdata.videotype" toggle-class="toggle-calm">Video Question</ion-toggle>',
            title: 'Advanced Filters',
            scope: $scope,
            cssClass: 'advanced-filter-popup',
            buttons: [
                {
                    text: '<b>Save</b>',
                    type: 'button-positive',
                    onTap: function (e) {

                        /*Check Full VIiw Image*/
                        if ($scope.editdata.imageview) {
                            $scope.editdata.imageview = 1;
                        } else {
                            $scope.editdata.imageview = 0;
                        };
                        /*Check Premium Question*/
                        if ($scope.editdata.premium) {
                            $scope.editdata.premium = 1;
                        } else {
                            $scope.editdata.premium = 0;
                        };
                        /*Check Trending Question*/
                        if ($scope.editdata.trending) {
                            $scope.editdata.trending = 1;
                        } else {
                            $scope.editdata.trending = 0;
                        };
                        /*Check Video Question*/
                        if ($scope.editdata.videotype) {
                            $scope.editdata.videotype = 1;
                            $scope.editdata.image = null;
                        } else {
                            $scope.editdata.videotype = 0;
                            $scope.editdata.video = null;
                        };
                    }
              }
            ]
        });
    };


    /*UPDATE EDIT*/
    var editquestionsuccess = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        if (data == '1') {
            edittype = '';
            editid = null;
            $location.path('/app/questions/:' + questionid);
        } else {
            /*ERROR COULD NOT EDIT QUESTION*/
        };
    };
    var editquestionerror = function (data, status) {
        $ionicLoading.hide();
        MyServices.showToast('There might be a problem with your Internet Connection');
        console.log(data);
        /*INTERNET ERROR*/
    };
    var editanswersuccess = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        if (data == '1') {
            edittype = '';
            editid = null;
            $location.path('/app/questions/:' + questionid);
        } else {
            /*ERROR COULD NOT EDIT ANSWER*/
            MyServices.showToast("Couldn't edit answer");
        };
    };
    var editanswererror = function (data, status) {
        $ionicLoading.hide();
        MyServices.showToast('There might be a problem with your Internet Connection');
        console.log(data);
        /*INTERNET ERROR*/
    };

    /*UPLOAD IMAGE*/
    $scope.imageuploaded = false;
    $scope.fileURI = '';

    $scope.parentmethod = function () {
        $scope.imageuploaded = true;
    };


    $scope.filetransferdone = function (data) {

        console.log("SUCCESS");
        console.log(data);
        $scope.editdata.image = data.response;

        if (edittype == "question") {
            console.log($scope.editdata);
            /*UPDATE QUESTION DATA*/
            MyServices.editquestion(questionid, $scope.editdata.qchapterid, $scope.editdata.question, $scope.editdata.image, $scope.editdata.imageview, $scope.editdata.premium, $scope.editdata.trending, $scope.editdata.video).success(editquestionsuccess).error(editquestionerror);
        } else {
            /*UPDATE ANSWER DATA*/
            MyServices.editanswer(editid, $scope.editdata.answer, $scope.editdata.image).success(editanswersuccess).error(editanswererror);
        };
    };

    /*FILE TRANSFER*/

    $scope.filetransfer = function () {

        var win = function (data, status) {
            $scope.fileURI = '';
            $ionicLoading.hide();
            $scope.filetransferdone(data);
        };
        var fail = function (error) {
            $ionicLoading.hide();
            console.log(error);
            MyServices.showToast('There seems to be a problem with the internet');
        };

        console.log($scope.fileURI);
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Uploading Image...'
        });

        $scope.imgname = $scope.fileURI.substr($scope.fileURI.lastIndexOf('/') + 1);

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = $scope.imgname;
        options.mimeType = "image/jpeg";

        var params = {};
        params.value1 = "test";
        params.value2 = "param";

        options.params = params;
        options.chunkedMode = false;

        console.log(options);

        /*if ($scope.fileURI.lastIndexOf('?') > 0) {
            $scope.fileURI.substr(0, $scope.fileURI.lastIndexOf('?'));
        };*/

        var ft = new FileTransfer();
        ft.upload($scope.fileURI, encodeURI("http://learnwithinq.com/appfilesv2/uploadimage.php"), win, fail, options, true);
    };

    $scope.edit = function () {
        if ($scope.imageuploaded == true) {
            if (edittype == "question") {

                /*CALL TO OTHER FUNCTION*/
                $scope.filetransfer();

            } else {
                if ($scope.editdata.answer != '') {

                    /*CALL TO OTHER FUNCTION*/
                    $scope.filetransfer();

                } else {
                    /*NO ANSWER ERROR*/
                    MyServices.showToast("Please type a answer");
                };
            };
        } else {
            $ionicLoading.show({
                template: '<ion-spinner class="android"></ion-spinner> <br>Saving changes'
            });
            if (edittype == "question") {
                console.log($scope.editdata);
                /*UPDATE QUESTION DATA*/
                MyServices.editquestion(questionid, $scope.editdata.qchapterid, $scope.editdata.question, $scope.editdata.image, $scope.editdata.imageview, $scope.editdata.premium, $scope.editdata.trending, $scope.editdata.video).success(editquestionsuccess).error(editquestionerror);

            } else {
                if ($scope.editdata.answer != '') {
                    /*UPDATE ANSWER DATA*/
                    MyServices.editanswer(editid, $scope.editdata.answer, $scope.editdata.image).success(editanswersuccess).error(editanswererror);
                } else {
                    /*NO ANSWER ERROR*/
                    MyServices.showToast("Please type a answer");
                };
            };
        };
    };

    function onDeviceReady() {
        pictureSource = navigator.camera.PictureSourceType;
        destinationType = navigator.camera.DestinationType;

        MyServices.viewanalyse("Edit Page");
    }

    function clearCache() {
        navigator.camera.cleanup();
    }

    var retries = 0;

    var onCapturePhoto = function (fileURI) {
        console.log(fileURI);
        $scope.fileURI = fileURI;

        /*CALL TO OTHER FUNCTION*/
        $scope.imageuploaded = true;

        $scope.showimg = true;
        var image = document.getElementById('editImage');
        console.log(image);
        if (image != null) {
            image.src = $scope.fileURI;

            $scope.$apply();
        };

        clearCache();

    };
    var onFail = function (message) {
        alert('Failed because: ' + message);
    };

    $scope.capturephoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            allowEdit: true,
            correctOrientation: true
        });
    };
    $scope.pickphoto = function () {
        navigator.camera.getPicture(onCapturePhoto, onFail, {
            quality: 30,
            destinationType: destinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            correctOrientation: true
        });
    };




    /*BOTTOM BAR FUNCTIONS*/
    $scope.selectallchaps = function () {
        for (var c = 0; c < $scope.chapters.length; c++) {
            $scope.chapters[c].selected = true;
        };
        $scope.allchapsbutton = false;
    };
    $scope.addtochapfilter = function () {
        $scope.allchapsbutton = false;
        for (var c = 0; c < $scope.chapters.length; c++) {
            if ($scope.chapters[c].selected == false) {
                $scope.allchapsbutton = true;
            } else {
                $scope.allchapsbutton = false;
            };
        };
    };

    $scope.removefilters = function () {
        $scope.showchapters = false;
    };

    $scope.selectsubject = function (index) {
        $scope.subjectname = $scope.subjects[index].subject;


        if ($scope.showchapters) {
            if ($scope.selectedsubject == index) {
                $scope.showchapters = false;
            } else {
                $scope.chaptername = '';
                $scope.editdata.chapterid = 0;
                $scope.chapters = $scope.subjects[index].chapters;
                $scope.selectedsubject = index;
            };
        } else {
            $scope.showchapters = true;

            if (index != $scope.selectedsubject) {
                $scope.chaptername = '';
                $scope.editdata.chapterid = 0;
            };
            $scope.chapters = $scope.subjects[index].chapters;
            $scope.selectedsubject = index;

        };



        $scope.subjects[index].isactive = true;

        /*get chapters*/
        var getchapterssuccess = function (data, status) {
            $scope.chapters = data;
        };
        var getchapterserror = function (data, status) {
            $scope.chapters = data;
        };
        //$scope.getchapters($scope.subjects[index].id).success(getchapterssuccess).error(getchapterserror);
    };

    /*FUNCTIONS*/

    /*SET CHAP ID*/
    $scope.setchapid = function (id) {
        console.log('chapter id is ' + id);
        $scope.editdata.chapterid = id;
    };

    /*SHOW CHAPTER*/
    $scope.selectchap = function (index) {
        $scope.chaptername = $scope.chapters[index].chaptertitle;
    };




    /*CANCEL ON LEAVE CLEAR CACHE*/
    $scope.$on('$ionicView.beforeLeave', function () {
        console.log('leaving');
        $scope.showimg = false;
        $scope.imageuploaded = false;
        $ionicHistory.clearCache();
    });

});

inqapp.controller('introCtrl', function ($scope, $stateParams, MyServices, $location, $filter, $ionicHistory, $ionicLoading, $ionicSlideBoxDelegate) {

    console.log("introduction");

    $scope.introslides = [
        {
            'introtitle': 'Ask A Question',
            'introimage': 'img/onbo/1.png'
        }, {
            'introtitle': 'Filter',
            'introimage': 'img/onbo/2.jpg'
        }, {
            'introtitle': 'Private Chat',
            'introimage': 'img/onbo/3.png'
        }, {
            'introtitle': 'FAB',
            'introimage': 'img/onbo/4.png'
        }, {
            'introtitle': 'Side Menu',
            'introimage': 'img/onbo/5.png'
        }, {
            'introtitle': 'Filter Options',
            'introimage': 'img/onbo/6.png'
        }, {
            'introtitle': 'Bookmark Questions',
            'introimage': 'img/onbo/7.png'
        }, {
            'introtitle': 'Answered Questions',
            'introimage': 'img/onbo/8.png'
        },
        {
            'introtitle': 'Verified Question Tag',
            'introimage': 'img/onbo/9.png'
        }
    ];

    $scope.gotologin = function () {
        console.log('skipped');
        $location.path('/app/login');
    };

    $scope.slideindex = 0;

    $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
        console.log('changed');

        for (cs = 0; cs < $scope.introslides.length; cs++) {
            var currenttitle = $('.swiper-slide-active .textcenter .introimage').attr('src');
            if (currenttitle == $scope.introslides[cs].introimage) {

                $scope.slideindex = cs;
                $scope.$apply();

            };
        };

    });

    $scope.next = function () {
        console.log("Go Next");

        $location.path('/app/login');

    };
});

inqapp.controller('leaderboardCtrl', function ($scope, $stateParams, MyServices, $location, $ionicLoading, $ionicSideMenuDelegate) {

    $scope.user = $.jStorage.get("user");
    $scope.usertypes = usertypes;

    $ionicSideMenuDelegate.canDragContent(true);

    $scope.showasked = true;

    $scope.changeleaderboard = function () {
        $scope.showasked = !$scope.showasked;
    };

    var gettopaskedsuccess = function (data, status) {
        $ionicLoading.hide();
        $scope.toptenasked = data;
        console.log(data);
    };
    var gettopaskederror = function (data, status) {
        $ionicLoading.hide();
        MyServices.showToast('There seems to be a problem with the internet');
        console.log(data);
    };
    var gettopansweredsuccess = function (data, status) {
        $ionicLoading.hide();
        $scope.toptenanswered = data;
        console.log(data);
    };
    var gettopanswerederror = function (data, status) {
        $ionicLoading.hide();
        MyServices.showToast('There seems to be a problem with the internet');
        console.log(data);
    };

    $scope.$on('$ionicView.beforeEnter', function () {
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Fetching Stats'
        });
        MyServices.gettopasked().success(gettopaskedsuccess).error(gettopaskederror);
        MyServices.gettopanswered().success(gettopansweredsuccess).error(gettopanswerederror);
    });
});

inqapp.controller('notificationsCtrl', function ($scope, $stateParams, MyServices, $location, $ionicLoading, $ionicSideMenuDelegate, $ionicModal, $filter) {
    $scope.user = $.jStorage.get("user");
    $scope.usertypes = usertypes;
    $scope.notification = {};
    //    var deviceid = $filter('getdeviceid')();
    var deviceid = $filter('getdeviceid')();
    $ionicSideMenuDelegate.canDragContent(true);

    $scope.notifications = [];

    /*CALLBACKS*/
    getnotificationsstatussuccess = function (data, status) {
        $scope.notification.isactive = data == "0" ? false : true;
    };
    getnotificationsstatuserror = function (data, status) {
        console.log("Internet Error");
    };

    $scope.togglenotificationstatus = function () {
        alert("TOGGLE");
    };


    var getnotificationssuccess = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
        $scope.notifications = data;
        if (data.length == 0) {
            $scope.loadmoreabe = false;
        };
    };
    var getnotificationserror = function (data, status) {
        $ionicLoading.hide();
        console.log(data);
    };

    var loadmorenotssuccess = function (data, status) {
        console.log(data);
        for (var p = 0; p < data.length; p++) {
            $scope.notifications.push(data[p]);
        };

        if (data.length == 0) {
            $scope.loadmoreabe = false;
        };
    };
    var loadmorenotserror = function (data, status) {
        console.log(data);
    };
    $scope.loadmoreabe = true;
    $scope.loadmorenots = function () {
        MyServices.getnotifications($scope.notifications.length).success(loadmorenotssuccess).error(loadmorenotserror);
    };
    $scope.$on('$ionicView.beforeEnter', function () {
        $ionicLoading.show({
            template: '<ion-spinner class="android"></ion-spinner> <br>Fetching Notifications'
        });
        MyServices.getnotifications(0).success(getnotificationssuccess).error(getnotificationserror);
        MyServices.getnotificationsstatus($scope.user.userid, deviceid).success(getnotificationsstatussuccess).error(getnotificationsstatuserror);
    });


    /*OPEN IMAGE POPUP*/

    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.openModal = function () {
        $scope.modal.show();
    };

    $scope.closeModal = function () {
        $scope.modal.hide();
    };


    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function () {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
        // Execute action
    });
    $scope.$on('modal.shown', function () {
        console.log('Modal is shown!');
    });
    $scope.showImage = function (img) {
        console.log(img);
        $scope.imageSrc = img;
        $scope.openModal();
    };


    /*STOP NOTIFICATION*/

    //    CALLBACK FUNCTION
    sendnotificationupdatesuccess = function (data, status) {
        //CHANGE NOTIFICATIONENABLED VALUE
        $scope.notification.isactive = data == "0" ? false : true;
        console.log(data, status);
    };

    sendnotificationupdateerror = function (data, status) {
        console.log("Internet Error");
    }

    $scope.sendnotificationupdate = function (sendnotification) {
        console.log($scope.notification.isactive);
        MyServices.sendnotificationupdate($scope.user.userid, deviceid).success(sendnotificationupdatesuccess).error(sendnotificationupdateerror);

    }



    /*DOWNLOAD IMAGE*/
    $scope.downloadimage = function () {
        console.log("downloading image");
        var fileTransfer = new FileTransfer();
        var image = $scope.imageSrc;
        var uri = encodeURI(image);
        var fileURL = cordova.file.externalRootDirectory + 'Download/' + $scope.imageSrc;

        fileTransfer.download(
            uri,
            fileURL,
            function (entry) {

                window.MediaScannerPlugin.scanFile(
                    function (msg) {
                        console.log(msg);
                    },
                    function (err) {
                        console.log(err);
                    }
                );

                var completePopup = $ionicPopup.alert({
                    title: 'Image Downloaded',
                    template: 'This image has been downloaded to your device memory',
                    cssClass: 'confirmpopup'
                });
                console.log("download complete: " + entry.toURL());
            },
            function (error) {
                console.log("download error source " + error.source);
                console.log("download error target " + error.target);
                console.log("download error code" + error.code);
                var completePopup = $ionicPopup.alert({
                    title: 'Image Not Downloaded',
                    template: 'This image could not be downloaded.Try Again.',
                    cssClass: 'confirmpopup'
                });
            },
            false, {
                headers: {
                    "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                }
            }
        );
    };

});

inqapp.controller('paymentCtrl', function ($scope, $stateParams) {


    $scope.purchaseobj = {
        numberofquestions: 0
    };

    $scope.unitquestioncost = 1;

    $scope.proceedtopayment = function () {
        console.log($scope.purchaseobj.numberofquestions * $scope.unitquestioncost);
    };

});
